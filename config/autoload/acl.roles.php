<?php

return array(

    'cajero'=> array(

        "allow"=>array(
            'ventas',
            'application',
            ''
        ),

        "deny"=>array(
            'recursos-humano',
            'compras',
            'configuracion',
            'almacen'
        )

    ),
    'contador'=> array(

        "allow"=>array(
            'compras',
            'recursos-humano',
            'reportes',
            ''
        ),

        "deny"=>array(
            'ventas',
            'configuracion',
            'almacen'
        )

    ),

    'almacenero'=> array(

        "allow"=>array(
            'almacen',
            ''
        ),

        "deny"=>array(
            'ventas',
            'configuracion',
            'reportes',
            'recursos-humano',
            'compras'
        )

    ),

    'administrador'=> array(

        "allow"=>array(

            'ventas',
            'recursos-humano',
            'compras',
            'configuracion',
            'almacen',
            'application',
            'reportes',
            ''

        ),

        "deny"=>array(

        )

    ),


    'visitante'=> array(

        "allow"=>array(
            'application',
            ''

        ),

        "deny"=>array(

            'ventas',
            'recursos-humano',
            'compras',
            'configuracion',
            'almacen',

        )

    ),
);

?>
