<?php
return array(

      //Definimos los accesos para la base de datos
    'db' => array(
        'driver' => 'Mysqli',
        'database' => 'db_soft24',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'options' => array('buffer_results' => true)
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'=> 'Zend\Db\Adapter\AdapterServiceFactory',
            'AuthService' => function($sm) {
                $authServiceManager = new \Zend\Authentication\AuthenticationService();
                // Obtenemos el adaptador de Base de datos
                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                // Configuramos el adaptador auth
                $authAdapter = new \Application\Authentication\Adapter\DbTableBcrypt($dbAdapter);
                $authAdapter->setTableName('tb_usuario')
                        ->setIdentityColumn('user_name')
                        ->setCredentialColumn('user_pass');
                // Y se lo pasamos a nuestro servicio
                $authServiceManager->setAdapter($authAdapter);
                return $authServiceManager;
            }
        ),
    ),


);