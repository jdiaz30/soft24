<?php

namespace Compras\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class ComprasPlugin extends AbstractPlugin {

	protected $productoTable;
    private $dbAdapter;
    protected $serviceLocator;

    public function __construct(ServiceLocator $serviceLocator){

        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');

        $this->productoTable = new \Almacen\Model\ProductoTable($this->dbAdapter);
        
    }

    public function getProductProveedor($idProv){

    	$htmlResponse = '';

    	$dataProd = $this->productoTable->getAllIdProveedor($idProv);

    	foreach ($dataProd as $data) {

    		$htmlResponse .= '<tr>';

    		$htmlResponse .= '<td data-title="Id">' .$data['id_prod'].'</td>';

    		$foto = "/img/thumbnail-default.jpg";

    		if ($data['foto'] != null || $data['foto']!="") {
    			$foto = "/img/productos/".$data['foto'];
    		}

            $igv = ($data['afecto_igv'] == "si") ? "si" : "no";


    		$htmlResponse .= '<td data-title="Foto"><img src='.$foto.' width="100"></td>';
    		$htmlResponse .= '<td data-title="Producto">' .$data['nom_prod'].'</td>';
    		$htmlResponse .= '<td data-title="Pre. Venta">' .$data['prec_vent'].'</td>';
    		$htmlResponse .= '<td data-title="Familia">' .$data['tipo_prod']." / ".$data['familia'].'</td>';
            $htmlResponse .= '<td data-title="Afecto IGV">' .$igv.'</td>';
    		$htmlResponse .= '<td class="text-center" data-title="Actions">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                       <li><a href="javascript:void(0)"><i class="icon-arrow-right15"></i> Agregar</a></li>
                                </ul>
                            </li>
                        </ul>
            </td>';

    		$htmlResponse .= '</tr>';

    		
    	}

    	return $htmlResponse;

    }


}