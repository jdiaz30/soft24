<?php

namespace Compras\View\Helper;

use Zend\View\Helper\AbstractHelper;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class CompraHelper extends AbstractHelper {

    private $dbAdapter;
    protected $serviceLocator;

    public function __construct(ServiceLocator $serviceLocator){
   
    }

    public function __invoke($option,$data = null){
       $response = "";

       switch ($option) {
         case 'estado':
           $response = $this->verEstado($data);
           break;
         
         default:
           # code...
           break;
       }

       return $response;
   
    }

    public function verEstado($data){
        $estado = "";

        switch ($data) {
          case '0':
            $estado = '<span class="label label-success">Activo</span>';
          break;
          case '1':
            $estado = '<span class="label label-danger">Inactivo</span>';
          break;

          default:
          # code...
          break;
        }

        return $estado;

    }


}

?>
