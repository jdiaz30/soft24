<?php

namespace Compras\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class CompraTable extends AbstractTableGateway {

    protected $table = 'tb_compra';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);

        return $this->lastInsertValue;
    }

    public function getNumFactProv($nFact,$idProv){

    	$expresion = new Expression("count(*)");

    	$sql = new Sql($this->adapter);

        $select = $sql->select();

        $select->from(array("c" => $this->table));

        $select->columns(array("total" => $expresion));
     
        $select->where(array("id_prov" => $idProv,"n_fact"=>$nFact));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;

    }

    public function getNumGuiaProv($nGuia,$idProv){

    	$expresion = new Expression("count(*)");

    	$sql = new Sql($this->adapter);

        $select = $sql->select();

        $select->from(array("c" => $this->table));

        $select->columns(array("total" => $expresion));
     
        $select->where(array("id_prov" => $idProv,"n_guia"=>$nGuia));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;

    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("c" => $this->table));
        $select->join(array("p"=>"tb_proveedor"),"c.id_prov = p.id_prov",array("razon_social"));

        $select->order("fecha_rec");

        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

}

?>
