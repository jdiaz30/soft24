<?php

namespace Compras\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class CompraProductoTable extends AbstractTableGateway {

    protected $table = 'tb_compra_producto';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos,$idCompras) {

    	foreach ($datos as $data) {

    		$data['id_compra'] = $idCompras;
    		$this->insert($data);
    	}
       
    }

}

?>
