<?php

namespace Compras\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;


class ComprasController extends MainController
{
	protected $usuarioSession;

	public function __construct(){

      $this->usuarioSession = $this->getUserSession();
        
    }

	public function indexAction(){

		$comprasData = $this->getComprasTable()->getAll();

		return new ViewModel(array(
			"compras" => $comprasData 
			));

	}

	public function addAction(){

		if ($this->getRequest()->isXmlHttpRequest()) {

			$data = $this->getRequest()->getPost();

			$productos = json_decode($data['productos'],true);

			$data['fecha_emi'] = date("Y-m-d", strtotime($data['fecha_emi']));;
			$data['fecha_rec'] = date("Y-m-d", strtotime($data['fecha_rec']));
			$data['estado'] = "0";
			$data['id_usuario'] = $this->usuarioSession->id;

            unset($data['tb_prod_comp_length']);
            unset($data['productos']);

			$idCompras = $this->getComprasTable()->add($data->toArray());

			$this->getCompraProductoTable()->add($productos,$idCompras);

			$this->getProductoTable()->updateStock($productos,"compras");

			//ACTUALIZAMOS EL PRECIO DE VENTA DEL PRODUCTO

			$this->getProductoTable()->updatePrecio($productos);

			return $this->getResponse()->setContent("0");
		}

		$proveedorData = $this->getProveedorTable()->getAllActive();
		$almacenData = $this->getAlmacenTable()->getAllActive();

        return new ViewModel(array(
            "proveedor" => $proveedorData,
            "almacen" => $almacenData
        ));
		
	}

	public function getProductByProvAction(){

		$data = $this->getRequest()->getPost();

        $dataProd = $this->comprasPlugin()->getProductProveedor($data['id_prov']);

        return $this->getResponse()->setContent($dataProd);

	}

	public function validaFacturaAction(){

		if ($this->getRequest()->isXmlHttpRequest()) {

			$data = $this->getRequest()->getPost();

			$valida = $this->getComprasTable()->getNumFactProv($data['n_fact'],$data['id_prov']);

			return $this->getResponse()->setContent($valida['total']);

		}
	}

	public function validaGuiaAction(){

		if ($this->getRequest()->isXmlHttpRequest()) {

			$data = $this->getRequest()->getPost();

			$valida = $this->getComprasTable()->getNumGuiaProv($data['n_guia'],$data['id_prov']);

			return $this->getResponse()->setContent($valida['total']);

		}
	}
 
}