<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Almacen\Controller\Index' => 'Almacen\Controller\IndexController',
            'Almacen\Controller\Producto' => 'Almacen\Controller\ProductoController',
            'Almacen\Controller\TipoProducto' => 'Almacen\Controller\TipoProductoController',
            'Almacen\Controller\FamiliaProducto' => 'Almacen\Controller\FamiliaProductoController',
            'Almacen\Controller\Proveedor' => 'Almacen\Controller\ProveedorController',
            'Almacen\Controller\Almacen' => 'Almacen\Controller\AlmacenController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'almacen' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/almacen',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Almacen\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]][/:id][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'almacen' => __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
    'invokables' => array(
        'funcionesHelper' => 'Almacen\View\Helper\Funciones',
    )
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'imagenPlugin' => 'Almacen\Plugin\ImagenPlugin',
           
        )
    ),
);
