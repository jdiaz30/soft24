<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Almacen;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        require_once( APPLICATION_PATH . "/../vendor/ZendImage/zendimage.php");
        require( APPLICATION_PATH . "/../vendor/Fpdf/fpdf.php");
        require( APPLICATION_PATH . "/../vendor/Fpdf/PDF.php");
        require_once( APPLICATION_PATH . "/../vendor/Money/EnLetras.php");
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Almacen\Model\FamiliaProductoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\FamiliaProductoTable($dbAdapter);

                    return $table;
                },
                'Almacen\Model\TipoProductoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\TipoProductoTable($dbAdapter);

                    return $table;
                },
                'Almacen\Model\UnidadMedidaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\UnidadMedidaTable($dbAdapter);

                    return $table;
                },
                'Almacen\Model\ProductoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\ProductoTable($dbAdapter);

                    return $table;
                },
                'Almacen\Model\ProveedorTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\ProveedorTable($dbAdapter);

                    return $table;
                },
                'Almacen\Model\ProvProductoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\ProvProductoTable($dbAdapter);

                    return $table;
                },
                'Almacen\Model\AlmacenTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Almacen\Model\AlmacenTable($dbAdapter);

                    return $table;
                },


                )
            );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
       
                'productoHelper' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Almacen\View\Helper\Producto($serviceLocator);
             
                }
            ),
        );
    }
}
