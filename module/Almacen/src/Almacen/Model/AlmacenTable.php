<?php

namespace Almacen\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class AlmacenTable extends AbstractTableGateway {

    protected $table = 'tb_almacen';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("a" => $this->table));
        $select->join(array("s"=>"tb_sucursal"),"s.id_sucursal = a.id_sucursal",array("sucursal"=>"descripcion"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("a" => $this->table));

        $select->where(array("estado"=>"0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getById($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();

        $select->from(array("a" => $this->table));
        $select->join(array("s"=>"tb_sucursal"),"s.id_sucursal = a.id_sucursal",array("sucursal"=>"descripcion","id_sucursal"));

        $select->where(array("a.id_almacen" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_almacen" => $datos['id_almacen']));
    }


  


}

?>
