<?php

namespace Almacen\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class UnidadMedidaTable extends AbstractTableGateway {

    protected $table = 'tb_unidad_med';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("u" => $this->table));

        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("u" => $this->table));

        $select->where(array("estado" => "0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }






}

?>
