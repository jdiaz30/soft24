<?php

namespace Almacen\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class TipoProductoTable extends AbstractTableGateway {

    protected $table = 'tb_tipo_prod';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("tp" => $this->table));

        $select->join(array("fp"=>"tb_familia_prod"),"tp.id_familia = fp.id_familia",array("nom_fam"=>"nombre"));

        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("fp" => $this->table));

        $select->where(array("estado" => "0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getAllId($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from(array("tp" => $this->table));

        $select->where(array("id_tipo_prod" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function getDataByFamId($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from(array("tp" => $this->table));

        $select->where(array("id_familia" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function edit($datos) {
        $this->update($datos, array("id_tipo_prod" => $datos['id_tipo_prod']));
    }

    public function remove($datos) {

        try {

            return $this->delete(array("id_tipo_prod" => $datos['id_tipo_prod']));
            
        } catch (\Exception $e) {

            return "error";
            
        }
        
    }


}

?>
