<?php

namespace Almacen\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class ProveedorTable extends AbstractTableGateway {

    protected $table = 'tb_proveedor';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("p" => $this->table));

        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("p" => $this->table));

        $select->where(array("estado" => "0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllId($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from($this->table);

        $select->where(array("id_prov" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_prov" => $datos['id_prov']));
    }

    public function remove($datos) {

        try {

            return $this->delete(array("id_prov" => $datos['id_prov']));
            
        } catch (\Exception $e) {

            return "error";
            
        }
        
    }

}

?>
