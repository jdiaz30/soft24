<?php

namespace Almacen\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class ProvProductoTable extends AbstractTableGateway {

    protected $table = 'tb_prov_prod';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {

        $prov = explode(",", $datos['prov']);

        foreach ($prov as $data) {

            $dataProvProd['id_prov'] = $data;
            $dataProvProd['id_prod'] = $datos['id_prod'];

           $this->insert($dataProvProd);
        }
        
    }

    public function getByIdProd($idProd){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("pv" => $this->table));

        $select->join(array("v"=>"tb_proveedor"),"v.id_prov = pv.id_prov",array("id_prov","razon_social","logo","estado"));

        $select->where(array("pv.id_prod" => $idProd));

        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function remove($datos) {
        $this->delete(array("id_prod" => $datos));
    }

}

?>
