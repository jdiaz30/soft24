<?php

namespace Almacen\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class ProductoTable extends AbstractTableGateway {

    protected $table = 'tb_producto';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);

        return $this->lastInsertValue;
    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("p" => $this->table));

        $select->join(array("tp"=>"tb_tipo_prod"),"tp.id_tipo_prod = p.id_tipo_prod",array("nom_tipo"=>"nombre"));
        $select->join(array("fp"=>"tb_familia_prod"),"fp.id_familia = tp.id_familia",array("nom_fam"=>"nombre"));

        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("fp" => $this->table));

        $select->where(array("estado" => "0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getAllByProv($idProv){

        $sql = new Sql($this->adapter);

        $select = $sql->select();

        $select->from(array("p" => $this->table));

        $select->join(array("pp"=>"tb_prov_prod"),"pp.id_prod = p.id_prod");

        $select->where(array("pp.id_prov" => $idProv));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getSinStock(){

        $expresion = new Expression("stock <= stock_min");

        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from(array("p" => $this->table));

        $select->where(array("estado" => "0"));

        $select->where($expresion);

        /*$select->where(array(
            new \Zend\Db\Sql\Predicate\PredicateSet(
                    array(
                      
                        new \Zend\Db\Sql\Predicate\Operator('stock', '<', 'stock_min'),
                      
                    )
            ),
        ));*/

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getAllId($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from($this->table);

        $select->where(array("id_prod" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_prod" => $datos['id_prod']));
    }

    public function getAllIdProveedor($idProv){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("p" => $this->table));

        $select->join(array("tp"=>"tb_tipo_prod"),"tp.id_tipo_prod = p.id_tipo_prod",array("tipo_prod"=>"nombre"));
        $select->join(array("fp"=>"tb_familia_prod"),"tp.id_familia = fp.id_familia",array("familia"=>"nombre"));

        $select->join(array("pp"=>"tb_prov_prod"),"pp.id_prod = p.id_prod");
        $select->join(array("pv"=>"tb_proveedor"),"pv.id_prov = pp.id_prov");

        //$select->columns(array("p.nom_prod"));

        $select->where(array("pp.id_prov" => $idProv,"p.estado" => "0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function updateStock($data , $opcion){

        $signo = ($opcion == "compras") ? "+" : "-";

        foreach ($data as $datos) {

            $expression = new \Zend\Db\Sql\Expression("stock".$signo ." ". ((int)$datos['cantidad']));

            $dataStock['stock'] = $expression;

            $this->update($dataStock, array("id_prod" => $datos['id_prod']));
             
        }

    }

    public function updatePrecio($data){

        foreach ($data as $datos) {

            $dataStock['prec_vent'] = $datos['prec_vent'];

            $this->update($dataStock, array("id_prod" => $datos['id_prod']));
             
        }

    }

    public function getAllByVentas(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("p" => $this->table));

        $select->join(array("tp"=>"tb_tipo_prod"),"tp.id_tipo_prod = p.id_tipo_prod",array("nom_tipo"=>"nombre"));
        $select->join(array("fp"=>"tb_familia_prod"),"fp.id_familia = tp.id_familia",array("nom_fam"=>"nombre"));

        $select->where(array("p.estado" => "0"));
       // $select->where->isNotNull("p.prec_vent");

       /* $select->where(array(
            new \Zend\Db\Sql\Predicate\PredicateSet(
                    array(
                      
                        new \Zend\Db\Sql\Predicate\Operator('p.stock', '!=', '0'),
                      
                    )
            ),
        ));*/

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }

    public function getByBarCode($barcode){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("p" => $this->table));

        $select->join(array("tp"=>"tb_tipo_prod"),"tp.id_tipo_prod = p.id_tipo_prod",array("nom_tipo"=>"nombre"));
        $select->join(array("fp"=>"tb_familia_prod"),"fp.id_familia = tp.id_familia",array("nom_fam"=>"nombre"));

        $select->where(array("p.cod_barras" => $barcode));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;

    }

    public function validaCodBarras($codBarras){

        $expresion = new Expression("count(*)");

        $sql = new Sql($this->adapter);

        $select = $sql->select();

        $select->from(array("p" => $this->table));

        $select->columns(array("total" => $expresion));

        $select->where(array("cod_barras"=>$codBarras));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;

    }

    public function remove($datos) {

        try {

            return $this->delete(array("id_prod" => $datos['id_prod']));
            
        } catch (\Exception $e) {

            return "error";
            
        }
        
    }



}

?>
