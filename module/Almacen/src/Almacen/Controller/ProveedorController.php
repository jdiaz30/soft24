<?php

namespace Almacen\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class ProveedorController extends MainController
{
    public function indexAction()
    {
        $proveedorData = $this->getProveedorTable()->getAll();

        return new ViewModel(array(
            "proveedor" =>$proveedorData
        ));

    }

    public function addAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getProveedorTable()->add($data->toArray());

            return $this->getResponse()->setContent("0");

        }

    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getProveedorTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $datos = $this->getProveedorTable()->getAllId($id);

        return new ViewModel(array(
            'datos'=>$datos
            ));

    }

    public function imageUploadAction(){

        $dir = APPLICATION_PATH ."/img/proveedores";
        $res = $this->imagenPlugin()->_uploadImage($dir);

        return $this->getResponse()->setContent($res);

    }

    public function deleteAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $res = $this->getProveedorTable()->remove($data->toArray());

            return $this->getResponse()->setContent($res);

        }

    }


}
