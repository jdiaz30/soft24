<?php

namespace Almacen\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class ProductoController extends MainController
{
    public function indexAction()
    {
        $productoData = $this->getProductoTable()->getAll();

        return new ViewModel(array(
        	"producto" =>$productoData
        ));
    }

    public function addAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $provData = $data['prov_prod'];

            unset($data['prov_prod']);
            unset($data['tb_prov_length']);

            $idProd = $this->getProductoTable()->add($data->toArray());

            if ($provData != "") {

                $provProd['prov'] = $provData;
                $provProd['id_prod'] = $idProd;

                $this->getProvProductoTable()->add($provProd);

            }

            return $this->getResponse()->setContent("0");
        
        }

        $proveedorData = $this->getProveedorTable()->getAllActive();

        return new ViewModel(array(
            "proveedor" =>$proveedorData
        ));

    }

    public function editAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $provData = $data['prov_prod'];

            unset($data['prov_prod']);
            unset($data['tb_prov_length']);


            $provProd['prov'] = $provData;
            $provProd['id_prod'] = $data['id_prod'];

            if ($provData != "") {

                $this->getProvProductoTable()->remove($data['id_prod']);

                $this->getProvProductoTable()->add($provProd);
            }

            $this->getProductoTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");
        
        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $datos = $this->getProductoTable()->getAllId($id);

        $datosProv = $this->getProvProductoTable()->getByIdProd($id);

        $proveedorData = $this->getProveedorTable()->getAllActive();

        return new ViewModel(array(
            'datos'=>$datos,
            'provProd'=>$datosProv,
            'proveedor'=>$proveedorData
            ));
    }

    public function imageUploadAction(){

    	$dir = APPLICATION_PATH ."/img/productos";
       	$res = $this->imagenPlugin()->_uploadImage($dir);

        return $this->getResponse()->setContent($res);

    }

    public function validaCodigoBarrasAction(){

        
        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $valida = $this->getProductoTable()->validaCodBarras($data['cod_barras']);

            return $this->getResponse()->setContent($valida['total']);

        }

    }

    public function deleteAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $res = $this->getProductoTable()->remove($data->toArray());

            return $this->getResponse()->setContent($res);

        }

    }


}
