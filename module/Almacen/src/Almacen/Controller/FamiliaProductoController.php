<?php

namespace Almacen\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class FamiliaProductoController extends MainController
{
    public function indexAction()
    {
        $data = $this->getFamiliaProductoTable()->getAll();

        return new ViewModel(array(
            'datos' => $data
            ));
    }

    public function addAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

            $this->getFamiliaProductoTable()->add($data->toArray());

    		return $this->getResponse()->setContent("0");

    	}
    	
    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getFamiliaProductoTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");
        
        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $datos = $this->getFamiliaProductoTable()->getAllId($id);

        return new ViewModel(array(
            'datos'=>$datos
            ));

    }

    public function deleteAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $res = $this->getFamiliaProductoTable()->remove($data->toArray());

            return $this->getResponse()->setContent($res);

        }

    }


}
