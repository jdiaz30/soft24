<?php

namespace Almacen\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class TipoProductoController extends MainController
{
    public function indexAction()
    {
       $data = $this->getTipoProductoTable()->getAll();

       return new ViewModel(array(
       	"data" => $data
       	));
    }

    public function addAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

    		$this->getTipoProductoTable()->add($data->toArray());

    		return $this->getResponse()->setContent("0");

		}

    	$familiaProd = $this->getFamiliaProductoTable()->getAllActive();

        return new  ViewModel(array(
        	"familia" => $familiaProd
        ));
    	
    }

    public function editAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

    		$this->getTipoProductoTable()->edit($data->toArray());

    		return $this->getResponse()->setContent("0");

    	}

    	$familiaProd = $this->getFamiliaProductoTable()->getAllActive();

    	$id = $this->getEvent()->getRouteMatch()->getParam('id');

        $tipoProd = $this->getTipoProductoTable()->getAllId($id);

        return new  ViewModel(array(
        	"familia" => $familiaProd,
        	"tipoProd" => $tipoProd
        ));

    }

    public function deleteAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $res = $this->getTipoProductoTable()->remove($data->toArray());

            return $this->getResponse()->setContent($res);

        }

    }


}
