<?php


namespace Almacen\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class AlmacenController extends MainController
{
    public function indexAction()
    {

        $almacen = $this->getAlmacenTable()->getAll();

        return new ViewModel(array(
            "almacen" => $almacen
            ));
      
    }

    public function addAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getAlmacenTable()->add($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $sucursal = $this->getSucursalTable()->getAllActive();

        return new ViewModel(array(
            "sucursal" => $sucursal
        ));
    
    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getAlmacenTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $datos = $this->getAlmacenTable()->getById($id);

        $sucursal = $this->getSucursalTable()->getAllActive();

        return new ViewModel(array(
            'datos'=>$datos,
            'sucursal' => $sucursal
            ));

    }

 

}
