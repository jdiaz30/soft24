<?php

namespace Almacen\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class Producto extends AbstractHelper {

  protected $famProdTable;
  protected $tipoProdTable;
  protected $unidMedTable;
  private $dbAdapter;
  protected $serviceLocator;

  public function __construct(ServiceLocator $serviceLocator){
    $this->serviceLocator = $serviceLocator;
    $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');

    $this->famProdTable = new \Almacen\Model\FamiliaProductoTable($this->dbAdapter);
    $this->tipoProdTable = new \Almacen\Model\TipoProductoTable($this->dbAdapter);
    $this->unidMedTable = new \Almacen\Model\UnidadMedidaTable($this->dbAdapter);

  }

  public function __invoke($option,$data = null){
    $response = "";

    switch ($option) {
      case 'select-tipo-prod':
        $response = $this->getSelectTipoProd($data);
      break;

      case 'select-unid-med':
        $response = $this->getSelectUnidMed($data);
      break;

      default:
          
      break;
    }

    return $response;

   }

  public function getSelectTipoProd($idTipoProd){

    $dataFamilia = $this->famProdTable->getAllActive();

    $htmlResponse = '';

    foreach ($dataFamilia as $familia) {

      $htmlResponse .='<optgroup label="'.$familia['nombre'].'">';

      $dataTipoProd = $this->tipoProdTable->getDataByFamId($familia['id_familia']);

      foreach ($dataTipoProd as $tipoProd) {

        if ($idTipoProd) {

          if($tipoProd['id_tipo_prod'] == $idTipoProd){

            $htmlResponse .= '<option value="'.$tipoProd['id_tipo_prod'].'" selected>'.$tipoProd['nombre'].'</option>';

          }else{

            $htmlResponse .= '<option value="'.$tipoProd['id_tipo_prod'].'">'.$tipoProd['nombre'].'</option>';

          }
          
        }else{
          $htmlResponse .= '<option value="'.$tipoProd['id_tipo_prod'].'">'.$tipoProd['nombre'].'</option>';
        }
        
      }

      $htmlResponse .='</optgroup>';
      
    }

    return $htmlResponse;

  }

  public function getSelectUnidMed($idUnidad){

    $dataUnidMed = $this->unidMedTable->getAllActive();

    $htmlResponse = '';

    foreach ($dataUnidMed as $data) {

      if ($idUnidad) {
        
        if ($data['id_unidad'] == $idUnidad) {
          $htmlResponse .= '<option value="'.$data['id_unidad'].'" selected >'.$data['nombre'].'</option>';
        }else{
          $htmlResponse .= '<option value="'.$data['id_unidad'].'">'.$data['nombre'].'</option>';
        }
      }else{

        $htmlResponse .= '<option value="'.$data['id_unidad'].'">'.$data['nombre'].'</option>';

      }
      
    }

    return $htmlResponse;
  }




}

?>
