<?php

namespace Almacen\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Funciones extends AbstractHelper {

    public function __invoke($option,$data){
       $response = "";

       switch ($option) {
         case 'estado':
           $response = $this->verEstado($data);
           break;
         
         default:
           # code...
           break;
       }

       return $response;

   
    }

    public function verEstado($data){
        $estado = "";

        switch ($data) {
          case '0':
            $estado = '<span class="label label-success">Activo</span>';
          break;
          case '1':
            $estado = '<span class="label label-danger">Inactivo</span>';
          break;

          default:
          # code...
          break;
        }

        return $estado;

    }


}

?>
