<?php

namespace Almacen\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\File\Transfer\Adapter\Http;

class ImagenPlugin extends AbstractPlugin {

    public function _uploadImage($path, $miniatura = false, $miniSize = null) {

        try {
            $adapter = new Http();
            $adapter->setDestination($path);

            $files = $adapter->getFileInfo();

            foreach ($files as $fieldName => $fieldInfo) {

                if (($adapter->isUploaded($fieldInfo['name'])) && ($adapter->isValid($fieldInfo['name']))) {
                    //$extension = substr($fieldInfo['name'], strrpos($fieldInfo['name'], '.') + 1);

                    $pathInfo = pathinfo($_FILES['userfile']['name']);

                    $extension = $pathInfo['extension'];

                    $nameNewFile = 'soft24';

                    $nameNewFile.= '_' . md5(microtime());

                    $nameNewFile.= '_' . rand(1000, 9999);
                    $nameNewFile.= '_.' . $extension;

                    //$filename = 'file_'.date('Ymdhs').'.'.$extension;
                    $fileOrigin = $path . '/' . $nameNewFile;
                    $adapter->addFilter(
                            'Rename', array(
                        'target' => $fileOrigin,
                        'overwrite' => true)
                    );

                    $adapter->receive($fieldInfo['name']);

                    if ($miniatura == true) {

                        $dirThumb = $path;
                        //@mkdir($dirThumb, 0755, true);

                        $zendImage = new \ZendImage();

                        //load file local
                        $dirFile = $dirThumb ."/". $nameNewFile;
                        $zendImage->loadImage($fileOrigin);

                        //resize file  by standard
                        $zendImage = $this->_resizeFile($zendImage, $miniSize);

                        $zendImage->save($dirFile, 100);
                    }

                    return $nameNewFile;
                }else{
                    echo "no hay nada";
                }
            }
        } catch (Exception $ex) {
            echo 'Exception!\n';
            echo $ex->getMessage();
        }
    }

    /* ----------------------------------------
     * Funcion que redimensiona una imagen
     * --------------------------------------- */

    public function _resize($imgOriginal, $imgNueva, $imgNuevaCalidad, $imgNuevaAnchura, $imgNuevaAltura) {
        $img = new ZendImage();
        $img->loadImage($imgOriginal);
        if ($img->width > $img->height)
            $img->resize($imgNuevaAnchura, 'width');
        else
            $img->resize($imgNuevaAltura, 'height');
            $img->save($imgNueva, $imgNuevaCalidad);
    }

    public function _resizeFile($zendImage, $configImageStandard) {
        if ($zendImage->width > $zendImage->height)
            $zendImage->resize($configImageStandard['w'], 'width');
        else
            $zendImage->resize($configImageStandard['h'], 'height');

        return $zendImage;
    }

}

?>
