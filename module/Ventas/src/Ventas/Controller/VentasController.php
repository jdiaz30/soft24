<?php

namespace Ventas\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;


class VentasController extends MainController
{
	protected $usuarioSession;
  protected $cajaSession;
  protected $auth;

	public function __construct(){

      $this->usuarioSession = $this->getUserSession();

      $this->cajaSession = new \Zend\Session\Container("caja");

      $this->auth = new \Zend\Authentication\AuthenticationService();
            
  }

	public function indexAction(){
	}

	public function addAction(){


    $identi = $this->auth->getStorage()->read();

		if ($this->getRequest()->isXmlHttpRequest()) {

			 $data = $this->getRequest()->getPost();
            
       $venta = $data['venta'];

       $venta['id_usuario'] = $this->usuarioSession->id;
       $venta['estado'] = "0";

       $ventaProd = $data['producto'];

       $idVenta = $this->getVentaTable()->add($venta);

       $this->getVentaProductoTable()->add($ventaProd,$idVenta);

       $ticketData['id_caja'] = $venta['id_caja'];
       $ticketData['n_ticket'] = $venta['ticket'];
       $ticketData['estado'] = "0";

       $this->ticketPlugin()->udpateTicket($ticketData);


       $caja = $this->getCajaTable()->getById($venta['id_caja']);

       $venta['caja'] = $caja['nom_caja'];
       $ticket = $this->ticket($venta,$ventaProd);

    
    		/*$printer = printer_open("HP Deskjet 2510 series");
    
   			printer_write($printer, $lipsum);
    
  
    		printer_close($printer);*/

    		//$this->imprimir();*/

        return $this->getResponse()->setContent($ticket);

		}

		$productos = $this->getProductoTable()->getAllByVentas();

		$cliente = $this->getClienteTable()->getAllActive();

    $tipoCambio = $this->getTipoCambioTable()->getByFecha(date("Y-m-d"));

		return new ViewModel(array(
			"productos" => $productos,
      "rol" => $identi->rol,
			"cliente" => $cliente,
      "tipoCambio" => $tipoCambio

			));
	}

	public function getProductoByBarCodeAction(){

		if ($this->getRequest()->isXmlHttpRequest()) {

			$data = $this->getRequest()->getPost();

      $producto = $this->getProductoTable()->getByBarCode($data['cod_barras']);

      $producto = json_encode($producto);

      return $this->getResponse()->setContent($producto);
		}
	}

	public function getClientesDniAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $cliente = $this->getClienteTable()->getAllActive();

            $htmlResponse = "";

            $htmlResponse.="<option value='0'> -- Elegir cliente -- </option>";

            foreach ($cliente as $data) {
              if ($data['dni'] !="") {
                $htmlResponse.="<option value=".$data['id_cliente']." data=".$data['nombre'].">".$data['dni']."</option>";
              }
             }
       
            return $this->getResponse()->setContent($htmlResponse);

        }
	}

  public function getClientesRucAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $cliente = $this->getClienteTable()->getAllActive();

            $htmlResponse = "";

            $htmlResponse.="<option value='0'> -- Elegir cliente -- </option>";

            foreach ($cliente as $data) {
              if ($data['ruc']) {

                $htmlResponse.="<option value=".$data['id_cliente']." data=".$data['nombre'].">".$data['ruc']."</option>";
  
              }
            }
       
            return $this->getResponse()->setContent($htmlResponse);

        }
  }

  public function getTicketAction(){

    $data = $this->getRequest()->getPost('caja');

    $caja = $this->getCajaTable()->getById($data);

    $ticket = $this->ticketPlugin()->getCodigoCorrelativo($caja);

    return $this->getResponse()->setContent($ticket);

  }

	public function reporteAction(){

    $pdf = new \PDF();
 
    $pdf->AddPage();

    $miCabecera = array('Ticket', 'F.Pago','Tipo Doc.', 'Total');

    $param['fecha_reg'] = $this->cajaSession->fecha;
    $param['hora_aper'] = $this->cajaSession->horaAper;
    $param['hora_cierre'] = $this->cajaSession->horaCierre;
    $param['id_caja'] = $this->cajaSession->caja;
 
    $misDatos = $this->getVentaTable()->getAllByCaja($param);

    $titulo = "Reporte de ventas " ." - caja (". $this->cajaSession->nomCaja . ")";

    $dataCaja = array();

    $dataCaja['id_usuario'] = $this->usuarioSession->id;
    $dataCaja['id_caja'] = $this->cajaSession->caja;
    $dataCaja['fecha'] = $this->cajaSession->fecha;
    $dataCaja['hora_aper'] = $this->cajaSession->horaAper;

    $montoApertura = $this->getMovCajaTable()->getCajaAperturada($dataCaja);
 
    $pdf->tablaHorizontal($miCabecera, $misDatos,$titulo,$montoApertura['monto_aper']);

    $pdf->Output(); //Salida al navegador

    $this->cajaSession->exchangeArray(array());
	}

	public function ticket($venta,$producto){

    $cliente = $this->getClienteTable()->getById($venta['id_cliente']);

    $moneyConverter = new \EnLetras();

    $total = $moneyConverter->ValorEnLetras($venta['total'], "Soles") ;

	  $htmlResponse = '<table width="80" border="0" style="font-size:12px;font-family:Arial">
    <tr>
      <td colspan="3" align="center" style="font-size:18px;">EFK Inversiones SAC</td>
    </tr>
    <tr>
      <td colspan="3" align="center" style="font-size:18px;">RUC : 20600711262</td>
    </tr>
    <tr>
      <td colspan="3" align="center">JR. ALFONSO UGARTE NRO. 145 LIMA - BARRANCA - BARRANCA </td>
    </tr>

    <tr>
      <td colspan="3" align="center" style="font-size:15px;">Ticket : '.$venta["ticket"].' </td>
    </tr>
    <tr>
      <td colspan="3" align="center" style="font-size:15px;">Fecha : '.$venta["fecha_reg"].'  Hora  : '.$venta['hora_reg'].'</td>
    </tr>
    <tr>
      <td colspan="3" align="center">0173845094999</td>
    </tr>
    <tr>
      <td colspan="3" align="center">Caja : '.$venta['caja'].'</td>
    </tr>';

    if ($venta['id_cliente'] != "0") {

    if ($venta['tipo_doc'] == "boleta") {

      $htmlResponse.='
      <tr>
        <td colspan="3">Cliente : '.$cliente['nombre'].'</td>
      </tr>';

      $htmlResponse.='
      <tr>
        <td colspan="3">Dni : '.$cliente['dni'].'</td>
      </tr>';
      
    }else{

      $htmlResponse.='
      <tr>
        <td colspan="3">Cliente : '.$cliente['nombre'].'</td>
      </tr>';

      $htmlResponse.='
      <tr>
      <td colspan="3">Ruc : '.$cliente['ruc'].'</td>
      </tr>';

      $htmlResponse.='
      <tr>
      <td colspan="3">Dirección : '.$cliente['direccion'].'</td>
      </tr>';

    }
   
    }

    $htmlResponse.='
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3"><table width="100%" border="0" class="productos" style="font-size:12px;font-family:Arial">
      <tr>
        <td>ITEM</td>
        <td>PRODUCTO</td>
        <td>CANT</td>
        <td>P.UNI</td>
        <td>TOTAL</td>
      </tr>';

      foreach ($producto as $item => $prod) {

        $item +=1;

        $dataProd = $this->getProductoTable()->getAllId($prod['id_prod']);
        
        $htmlResponse .='<tr>';
        $htmlResponse .='<td>'.$item.'</td>';
        $htmlResponse .='<td>'.$dataProd['nom_prod'].'</br>'.$dataProd['cod_barras'].'</td>';
        $htmlResponse .='<td>'.$prod['cantidad'].'</td>';
        $htmlResponse .='<td>'.$prod['precio'].'</td>';
        $htmlResponse .='<td>'.$prod['subtotal'].'</td>';

        $htmlResponse .='</tr>';
      }

    $htmlResponse .='
   
    </table></td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" style="font-size:14px;">SUBTOTAL :  '.$venta['subtotal'].'</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" style="font-size:14px;">IGV : '.$venta['igv'].'</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right" style="font-size:14px;">TOTAL : '.$venta['total'].'</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td colspan="3" align="center">SON : '.$total.'</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    <tr>
    <td colspan="3" align="center">Cajero : '.$this->usuarioSession->nombre.' </td>
    </tr>

    <tr>
     <td>&nbsp;</td>
    </tr>

    <tr>
    <td colspan="3" align="center">GRACIAS POR SU COMPRA</td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    <tr>
     <td>&nbsp;</td>
    </tr>
    </table>';

    return $htmlResponse;
	}



 
}