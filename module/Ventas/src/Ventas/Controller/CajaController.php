<?php

namespace Ventas\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;


class CajaController extends MainController
{

	protected $usuarioSession;

	public function __construct(){

      $this->usuarioSession = $this->getUserSession();
      $this->cajaSession = new \Zend\Session\Container("caja");
        
    }

	public function indexAction(){

	}

	public function addAction(){


        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $data['id_usuario'] = $this->usuarioSession->id;
            $data['estado'] = "0";
            $data['monto_aper'] = str_replace(",", "",$data['monto_aper']);

            $this->getMovCajaTable()->add($data->toArray());

            $nomCaja = $caja = $this->getCajaTable()->getById($data['id_caja']);

            $data['nom_caja'] = $nomCaja['nom_caja'];

            $dataJson = json_encode($data);

           /*$cajaSession = new \Zend\Session\Container("caja");

            $cajaSession->apertura = "si";
            $cajaSession->usuario = $this->usuarioSession->id;
            $cajaSession->caja = $data['id_caja'];
            $cajaSession->nomCaja = $nomCaja['nom_caja'];*/

            return $this->getResponse()->setContent($dataJson);

        }

	}

    public function cerrarAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost('cierre');

            $caja = $this->getRequest()->getPost('caja');

            $data['estado'] = "1";//cerrada

            $this->getMovCajaTable()->edit($data);

            $this->cajaSession->caja = $caja['id_caja'];
            $this->cajaSession->nomCaja = $caja['nom_caja'];
            $this->cajaSession->horaAper = $caja['hora_aper'];
            $this->cajaSession->horaCierre = $data['hora_cierre'];
            $this->cajaSession->fecha = $data['fecha'];

            return $this->getResponse()->setContent("0");

        }

    }

}