<?php

namespace Ventas\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\File\Transfer\Adapter\Http;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class MovimientoCajaPlugin extends AbstractPlugin {

	protected $serviceLocator;
	private $dbAdapter;
	protected $movCajaTable;
	protected $cajaTable;


    public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');  

        $this->cajaTable = new \Configuracion\Model\CajaTable($this->dbAdapter);
        $this->movCajaTable = new \Ventas\Model\MovimientoCajaTable($this->dbAdapter);
    }

    public function getCajaDisponible(){
    	
    	$caja = $this->cajaTable->getAllActive();
        
        $hoy = date("Y/m/d");

    	$cajaAperturada = $this->movCajaTable->getCajaAperturadas($hoy);

    	$cajaDisp = array();
    	$cajaApe = array();

    	foreach ($cajaAperturada as $apertura) {
    		
    		$cajaApe[] = $apertura['id_caja'];
    	}

    	foreach ($caja as $data) {
    		
    		if (!in_array($data['id_caja'], $cajaApe)) {

    			$cajaDisp[] = $data;

    		}
    	}

    	return $cajaDisp;

  
    }



}