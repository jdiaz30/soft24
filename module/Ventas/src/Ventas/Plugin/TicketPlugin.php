<?php

namespace Ventas\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\File\Transfer\Adapter\Http;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class TicketPlugin extends AbstractPlugin {

	protected $serviceLocator;
	private $dbAdapter;
	protected $ticketTable;


    public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');  

        $this->ticketTable = new \Ventas\Model\TicketTable($this->dbAdapter);

    }

    public function getCodigoCorrelativo($caja){

        $codigo = $this->ticketTable->getByCaja($caja['id_caja']);

        $codigoGen = "";
        $nomCaja = $caja['nom_caja'];

        $total = count($codigo);

        if ($total > 0 ) {

            $pos = strpos( $codigo['n_ticket'],"-");

            $cod = substr($codigo['n_ticket'], $pos +1);

            $cod =  $cod + 1;

            switch ($cod) {
                case $cod < 10:
                    $codigoGen = $nomCaja ."-"."000000".$cod;
                    break;

                case $cod >= 10 && $cod <= 99:
                    $codigoGen = $nomCaja ."-"."00000".$cod;
                    break;

                case $cod >= 100 && $cod <= 999:
                    $codigoGen = $nomCaja ."-"."0000".$cod;
                    break;

                case $cod >= 1000 && $cod <= 9999:
                    $codigoGen = $nomCaja ."-"."000".$cod;
                    break;

                case $cod >= 10000 && $cod <= 99999:
                    $codigoGen = $nomCaja ."-"."00".$cod;
                    break;

                case $cod >= 100000 && $cod <= 999999:
                    $codigoGen = $nomCaja ."-"."0".$cod;
                    break;

                case $cod >= 1000000 && $cod <= 9999999:
                    $codigoGen = $nomCaja ."-".$cod;
                    break;

                default:
                    # code...
                    break;
            }
            
        }else{

            $codigoGen = $nomCaja ."-"."0000001";

        }

        return $codigoGen;

    }

    public function udpateTicket($data){

        $codigo = $this->ticketTable->getByCaja($data['id_caja']);

        $total = count($codigo);

        if ($total > 0  && $codigo != "") {

            $data['id_ticket'] = $codigo['id_ticket'];

            $this->ticketTable->edit($data);
            
        }else{

            $this->ticketTable->add($data);

        }

    }


}