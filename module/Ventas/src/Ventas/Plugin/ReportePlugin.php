<?php

namespace Ventas\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\File\Transfer\Adapter\Http;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class ReportePlugin extends AbstractPlugin {

	protected $serviceLocator;
	private $dbAdapter;
	protected $ventaTable;


    public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');  

        $this->ventaTable = new \Ventas\Model\VentaTable($this->dbAdapter);

    }




}