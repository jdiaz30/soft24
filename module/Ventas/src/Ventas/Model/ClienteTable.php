<?php

namespace Ventas\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class ClienteTable extends AbstractTableGateway {

    protected $table = 'tb_cliente';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAllActive(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("c" => $this->table));
       
        $select->where(array("estado"=>"0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getById($id){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("c" => $this->table));

        $select->where(array("id_cliente"=>$id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }




}

?>
