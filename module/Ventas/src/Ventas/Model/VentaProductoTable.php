<?php

namespace Ventas\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class VentaProductoTable extends AbstractTableGateway {

    protected $table = 'tb_venta_producto';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos,$idVenta) {
    	
        foreach ($datos as $data) {

    		$data['id_venta'] = $idVenta;
    		$this->insert($data);
    	}
    }

}

?>
