<?php

namespace Ventas\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class MovimientoCajaTable extends AbstractTableGateway {

    protected $table = 'tb_movimiento_caja';

    // estado  = 0 => aperturada
    // estado = 1 => finalizada o cerrada

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getCajaAperturadas($fecha){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("c" => $this->table));
       
        $select->where(array("fecha"=>$fecha));
        $select->where(array("estado"=>"0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getCajaAperturada($data){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("c" => $this->table));
       
        $select->where(array("fecha"=>$data['fecha']));
        $select->where(array("hora_aper"=>$data['hora_aper']));
        $select->where(array("id_usuario"=>$data['id_usuario']));
        $select->where(array("id_caja"=>$data['id_caja']));
    
       // $select->where(array("estado"=>"0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_caja" => $datos['id_caja'],"id_usuario" => $datos['id_usuario'],"fecha"=>$datos['fecha'],"estado"=>"0"));
    }


}

?>
