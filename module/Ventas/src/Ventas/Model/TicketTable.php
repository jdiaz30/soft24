<?php

namespace Ventas\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class TicketTable extends AbstractTableGateway {

    protected $table = 'tb_ticket';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getByCaja($caja){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("t" => $this->table));

        $select->where(array("id_caja"=>$caja));
       
        $select->where(array("estado"=>"0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_ticket" => $datos['id_ticket']));
    }

 




}

?>
