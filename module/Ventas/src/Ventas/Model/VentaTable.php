<?php

namespace Ventas\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class VentaTable extends AbstractTableGateway {

    protected $table = 'tb_venta';

    //echo "demo" .$sql->getSqlstringForSqlObject($select);

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);

        return $this->lastInsertValue;
    }

    public function getAllByCaja($data){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("v" => $this->table));
       
        $select->where(array("estado"=>"0"));
        $select->where(array("id_caja"=> $data['id_caja']));

        $select->where(array("fecha_reg"=> $data['fecha_reg']));

        $select->where(array(
            new \Zend\Db\Sql\Predicate\PredicateSet(
                    array(
                      
                        new \Zend\Db\Sql\Predicate\Operator('hora_reg', '>=', $data['hora_aper']),
                        new \Zend\Db\Sql\Predicate\Operator('hora_reg', '<=', $data['hora_cierre']),
                      
                    )
            ),
        ));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllByFecha($data){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("v" => $this->table));

        $select->join(array("u"=>"tb_usuario"),"v.id_usuario = u.id_usuario",array("user_name"));
        $select->join(array("e"=>"tb_empleado"),"e.id_empleado = u.id_empleado",array("nombre","ap_pat"));
        $select->join(array("c"=>"tb_caja"),"v.id_caja = c.id_caja",array("nom_caja"));
       
        $select->where(array("v.estado"=>"0"));
        //$select->where(array("id_caja"=> $data['id_caja']));

        //$select->where(array("fecha_reg"=> $data['fecha_reg']));

        $select->where(array(
            new \Zend\Db\Sql\Predicate\PredicateSet(
                    array(
                      
                        new \Zend\Db\Sql\Predicate\Operator('v.fecha_reg', '>=', $data['fecha_ini']),
                        new \Zend\Db\Sql\Predicate\Operator('v.fecha_reg', '<=', $data['fecha_fin']),
                      
                    )
            ),
        ));

        $select->order(array("v.f_pago","v.id_caja"));
        //$select->group("tipo_doc");

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllByCajaApertura($data){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("v" => $this->table));

        $select->join(array("u"=>"tb_usuario"),"v.id_usuario = u.id_usuario",array("user_name"));
        $select->join(array("e"=>"tb_empleado"),"e.id_empleado = u.id_empleado",array("nombre","ap_pat"));
        $select->join(array("c"=>"tb_caja"),"v.id_caja = c.id_caja",array("nom_caja"));
       
        $select->where(array("v.estado"=>"0"));
        $select->where(array("v.id_caja"=> $data['id_caja']));

        $select->where(array("v.fecha_reg"=> $data['fecha_reg']));

        $select->where(array(
            new \Zend\Db\Sql\Predicate\PredicateSet(
                    array(
                      
                        new \Zend\Db\Sql\Predicate\Operator('v.hora_reg', '>=', $data['hora_aper']),
                        new \Zend\Db\Sql\Predicate\Operator('v.hora_reg', '<=', $data['hora_cierre']),
                      
                    )
            ),
        ));

        $select->order(array("v.f_pago","v.id_caja"));
        //$select->group("tipo_doc");

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

}

?>
