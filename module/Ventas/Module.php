<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ventas;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

       /* $authListener = new \Application\Authentication\AuthenticationListener();
        $authListener->attach($eventManager);*/
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Ventas\Model\ClienteTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Ventas\Model\ClienteTable($dbAdapter);

                    return $table;
                },
                'Ventas\Model\VentaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Ventas\Model\VentaTable($dbAdapter);

                    return $table;
                },
                'Ventas\Model\VentaProductoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Ventas\Model\VentaProductoTable($dbAdapter);

                    return $table;
                },
                'Ventas\Model\MovimientoCajaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Ventas\Model\MovimientoCajaTable($dbAdapter);

                    return $table;
                },
                'Ventas\Model\TicketTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Ventas\Model\TicketTable($dbAdapter);

                    return $table;
                },
    
         
                )
            );
    }

    public function getControllerPluginConfig(){
        return array(
            'factories' => array(
                'movCajaPlugin' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Ventas\Plugin\MovimientoCajaPlugin($serviceLocator);

                },
                'ticketPlugin' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Ventas\Plugin\TicketPlugin($serviceLocator);

                }
              
                ),
            );
    }
}
