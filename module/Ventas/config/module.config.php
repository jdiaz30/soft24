<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Ventas\Controller\Index' => 'Ventas\Controller\IndexController',
            'Ventas\Controller\Ventas' => 'Ventas\Controller\VentasController',
            'Ventas\Controller\Cliente' => 'Ventas\Controller\ClienteController',
            'Ventas\Controller\Caja' => 'Ventas\Controller\CajaController',
            
        ),
    ),
    'router' => array(
        'routes' => array(
            'ventas' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/ventas',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Ventas\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Ventas' => __DIR__ . '/../view',
        ),
    ),
);
