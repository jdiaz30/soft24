<?php

namespace Reportes\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;


class ProductoController extends MainController
{
    public function indexAction()
    {

    	$proveedorData = $this->getProveedorTable()->getAllActive();

        return new ViewModel(array(
            "proveedor" =>$proveedorData
        ));
      
    }

    public function stockAction(){

    	$reporte = $this->productoPlugin()->reporteStock();

    	return $this->getResponse()->setContent(0);
    }

    public function sinStockAction(){

    	$reporte = $this->productoPlugin()->reporteSinStock();

    	return $this->getResponse()->setContent(0);

    }

    public function proveedorAction(){

    	$proveedor = $this->getEvent()->getRouteMatch()->getParam('id');

    	$reporte = $this->productoPlugin()->reporteProveedor($proveedor);

    	return $this->getResponse()->setContent(0);

    }





 
}
