<?php

namespace Reportes\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;


class VentasController extends MainController
{
    public function indexAction(){

    }

    public function fechaAction(){

        $data['fecha_ini'] = $this->getEvent()->getRouteMatch()->getParam('fecha_ini');
        $data['fecha_fin'] = $this->getEvent()->getRouteMatch()->getParam('fecha_fin');

        $reporte = $this->ventasPlugin()->reporteXFecha($data);

    	return $this->getResponse()->setContent(0);
    }

    public function cajaAction(){

        $data['fecha_reg'] = "2016-01-04";
        $data['hora_aper'] = "10:05:27 PM";
        $data['hora_cierre'] = "2:59:35 AM";
        $data['id_caja'] = "3";
        $data['nom_caja'] = "002";

        $reporte = $this->ventasPlugin()->reporteXCaja($data);

        return $this->getResponse()->setContent(0);
    }





 
}
