<?php

namespace Reportes\Plugin;

use Reportes\Plugin\Main\MainPlugin;
use Zend\File\Transfer\Adapter\Http;

class VentasPlugin extends MainPlugin {

	protected $serviceLocator;
	private $dbAdapter;


    public function reporteXFecha($fecha){

    	$this->cabecera("Reporte - Ventas");

        $this->pdf->SetXY(10, 20);
        $this->pdf->SetFont('Arial','',11);

        $fechaTitulo = "del " .$fecha['fecha_ini']." al ".$fecha['fecha_fin'];

        $this->pdf->Cell(0,10,$fechaTitulo,0,0,'C');


        $miCabecera = array('Item', 'Ticket','Vendedor','Fecha', 'Caja','Doc.','F.Pago','Tarjeta', 'Total');

        $this->cabeceraTabla($miCabecera,16,30,20,7);

        //llenamos la data

        $this->pdf->SetXY(16,37);
        $this->pdf->SetFont('Arial','',10);
        $this->pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $item = 1;

        $fill = 37;

        $dataProducto = $this->ventaTable->getAllByFecha($fecha);

        $totalVenta = 0;

        foreach($dataProducto as $fila)
        {

        	$this->CellFitSpace(20,7, utf8_decode($item),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(20,7, utf8_decode($fila['ticket']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['nombre'] ." ". substr($fila['ap_pat'],0,3)."."),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(20,7, utf8_decode($fila['fecha_reg']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(20,7, utf8_decode($fila['nom_caja']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(20,7, utf8_decode($fila['tipo_doc']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['f_pago']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['tarjeta']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['total']),1, 0 , 'C', $bandera );

            $this->pdf->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $totalVenta += $fila['total'];

            $item+= 1;

            $fill+=7;

            $this->pdf->SetXY(16,$fill);
        }

        //Salto de línea
        $this->pdf->Ln();

        $this->pdf->Cell(0,7,"Total venta : " ."S/.". $totalVenta ,0,0,'C');

        $this->pdf->Output(); 

    }

    public function reporteXCaja($caja){

        $this->cabecera("Reporte - Ventas Caja (".$caja['nom_caja'].")");

        $this->pdf->SetXY(10, 20);
        $this->pdf->SetFont('Arial','',11);

        $fechaTitulo = $caja['fecha_reg'] ." hora de apertura : " .$caja['hora_aper']." ". "hora de cierre " .$caja['hora_cierre'];

        $this->pdf->Cell(0,10,$fechaTitulo,0,0,'C');

        $this->pdf->SetXY(10, 27);

        $this->pdf->Cell(0,10,"Josue Diaz",0,0,'C');


        $miCabecera = array('Item', 'Ticket','Fecha','Hora', 'Caja','Doc.','F.Pago','Tarjeta', 'Total');

        $this->cabeceraTabla($miCabecera,17,38,20,7);

        //llenamos la data

        $this->pdf->SetXY(17,45);
        $this->pdf->SetFont('Arial','',10);
        $this->pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $item = 1;

        $fill = 45;

        $dataProducto = $this->ventaTable->getAllByCajaApertura($caja);

        $totalVenta = 0;

        foreach($dataProducto as $fila)
        {

            $this->CellFitSpace(20,7, utf8_decode($item),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['ticket']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['fecha_reg']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['hora_reg']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['nom_caja']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['tipo_doc']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['f_pago']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['tarjeta']),1, 0 , 'C', $bandera );
            $this->CellFitSpace(20,7, utf8_decode($fila['total']),1, 0 , 'C', $bandera );

            $this->pdf->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $totalVenta += $fila['total'];

            $item+= 1;

            $fill+=7;

            $this->pdf->SetXY(17,$fill);
        }

        //Salto de línea
        $this->pdf->Ln();

        $this->pdf->Cell(0,7,"Total venta : " ."S/.". $totalVenta ,0,0,'C');

        $this->pdf->Output(); 

    }

 

    public function reporteSinStock(){

    	$this->cabecera("Reporte - Productos sin stock");

        $miCabecera = array('Item', 'Cod.Barras','Producto.', 'Stock' ,'Stock min.');

        $this->cabeceraTabla($miCabecera,30,30,30,7);

        //llenamos la data

        $this->pdf->SetXY(30,37);
        $this->pdf->SetFont('Arial','',10);
        $this->pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $item = 1;

        $fill =37;

        $dataProducto = $this->productoTable->getSinStock();

        $totalProd = count($dataProducto);

        foreach($dataProducto as $fila)
        {

        	$this->CellFitSpace(30,7, utf8_decode($item),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['cod_barras']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['nom_prod']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['stock']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['stock_min']),1, 0 , 'C', $bandera );

            $this->pdf->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $item+= 1;

            $fill+=7;

            $this->pdf->SetXY(30,$fill);
        }


        //Salto de línea
        $this->pdf->Ln();

        $this->pdf->Cell(0,7,"Total productos : " . $totalProd ,0,0,'C');

        $this->pdf->Output(); 

    }

    public function cabecera($titulo){

    	$fecha = date("Y-m-d");

    	$this->pdf->AddPage();

    	$this->pdf->SetXY(160, 3);
        $this->pdf->SetFont('Arial','B',10);

        $this->pdf->Cell(0,10,"Fecha : ".$fecha,0,0,'LR');

        $this->pdf->SetXY(10, 12);
        $this->pdf->SetFont('Arial','B',15);

        $this->pdf->Cell(0,10,$titulo,0,0,'C');

    }

    public  function cabeceraTabla($cabecera,$x,$y,$ancho,$alto)
    {

    	$this->pdf->SetXY($x, $y);
    	$this->pdf->SetFont('Arial','B',10);
        $this->pdf->SetFillColor(2,157,116);//Fondo verde de celda
        $this->pdf->SetTextColor(240, 255, 240); //Letra color blanco

        foreach($cabecera as $fila)
        {
            //Atención!! el parámetro true rellena la celda con el color elegido
        	$this->CellFitSpace($ancho,$alto, utf8_decode($fila),1, 0 , 'C', true);
        }
    }



}