<?php

namespace Reportes\Plugin;

use Reportes\Plugin\Main\MainPlugin;
use Zend\File\Transfer\Adapter\Http;

class ProductoPlugin extends MainPlugin {

	protected $serviceLocator;
	private $dbAdapter;
	//public $pdf;

   /* public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');  
 
    }*/

    public function reporteProveedor($idProv){

    	$this->cabecera("Reporte - Productos x Proveedor");

    	$dataProv = $this->proveedorTable->getAllId($idProv);

    	$this->pdf->SetXY(10, 20);
        $this->pdf->SetFont('Arial','',13);

        $this->pdf->Cell(0,10,$dataProv['razon_social'],0,0,'C');

        $miCabecera = array('Item', 'Cod.Barras','Producto.', 'Stock', 'Precio Unit');

        $this->cabeceraTabla($miCabecera,20,30,35,7);

        //llenamos la data

        $this->pdf->SetXY(20,37);
        $this->pdf->SetFont('Arial','',9);
        $this->pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $item = 1;

        $fill = 37;

        $dataProducto = $this->productoTable->getAllByProv($idProv);

        $totalProd = count($dataProducto);

        foreach($dataProducto as $fila)
        {

        	$this->CellFitSpace(35,7, utf8_decode($item),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode($fila['cod_barras']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode(substr($fila['nom_prod'], 0,10)."."),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode($fila['stock']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode($fila['prec_vent']),1, 0 , 'C', $bandera );

            $this->pdf->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $item+= 1;

            $fill+=7;

            $this->pdf->SetXY(20,$fill);
        }

        //Salto de línea
        $this->pdf->Ln();

        $this->pdf->Cell(0,7,"TOTAL PRODUCTOS : " . $totalProd ,0,0,'C');

        $this->pdf->Output(); 

    }

    public function reporteStock(){

    	$this->cabecera("Reporte - stock de productos");

        $miCabecera = array('Item', 'Cod.Barras','Producto.', 'Stock');

        $this->cabeceraTabla($miCabecera,10,30,35,7);

        //llenamos la data

        $this->pdf->SetXY(0,37);
        $this->pdf->SetFont('Arial','',10);
        $this->pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $item = 1;

        $fill =37;

        $dataProducto = $this->productoTable->getAllActive();

        $totalProd = count($dataProducto);

        foreach($dataProducto as $fila)
        {

        	$this->CellFitSpace(35,7, utf8_decode($item),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode($fila['cod_barras']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode(substr($fila['nom_prod'], 0,10)."."),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(35,7, utf8_decode($fila['stock']),1, 0 , 'C', $bandera );

            $this->pdf->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $item+= 1;

            $fill+=7;

            //$this->pdf->SetXY(37,$fill);
        }


        //Salto de línea
        $this->pdf->Ln();

        $this->pdf->Cell(0,7,"Total productos : " . $totalProd ,0,0,'C');

        $this->pdf->Output(); 

    }

    public function reporteSinStock(){

    	$this->cabecera("Reporte - Productos sin stock");

        $miCabecera = array('Item', 'Cod.Barras','Producto.', 'Stock' ,'Stock min.');

        $this->cabeceraTabla($miCabecera,30,30,30,7);

        //llenamos la data

        $this->pdf->SetXY(30,37);
        $this->pdf->SetFont('Arial','',10);
        $this->pdf->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->pdf->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $item = 1;

        $fill =37;

        $dataProducto = $this->productoTable->getSinStock();

        $totalProd = count($dataProducto);

        foreach($dataProducto as $fila)
        {

        	$this->CellFitSpace(30,7, utf8_decode($item),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['cod_barras']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['nom_prod']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['stock']),1, 0 , 'C', $bandera );
        	$this->CellFitSpace(30,7, utf8_decode($fila['stock_min']),1, 0 , 'C', $bandera );

            $this->pdf->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $item+= 1;

            $fill+=7;

            $this->pdf->SetXY(30,$fill);
        }


        //Salto de línea
        $this->pdf->Ln();

        $this->pdf->Cell(0,7,"Total productos : " . $totalProd ,0,0,'C');

        $this->pdf->Output(); 

    }

    public function cabecera($titulo){

    	$fecha = date("Y-m-d");

    	$this->pdf->AddPage();

    	$this->pdf->SetXY(160, 3);
        $this->pdf->SetFont('Arial','B',10);

        $this->pdf->Cell(0,10,"Fecha : ".$fecha,0,0,'LR');

        $this->pdf->SetXY(10, 12);
        $this->pdf->SetFont('Arial','B',15);

        $this->pdf->Cell(0,10,$titulo,0,0,'C');

    }

    public  function cabeceraTabla($cabecera,$x,$y,$ancho,$alto)
    {

    	$this->pdf->SetXY($x, $y);
    	$this->pdf->SetFont('Arial','B',10);
        $this->pdf->SetFillColor(2,157,116);//Fondo verde de celda
        $this->pdf->SetTextColor(240, 255, 240); //Letra color blanco

        foreach($cabecera as $fila)
        {
            //Atención!! el parámetro true rellena la celda con el color elegido
        	$this->CellFitSpace($ancho,$alto, utf8_decode($fila),1, 0 , 'C', true);
        }
    }



}