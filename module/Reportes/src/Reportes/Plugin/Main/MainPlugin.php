<?php

namespace Reportes\Plugin\Main;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\File\Transfer\Adapter\Http;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class MainPlugin extends AbstractPlugin {

	public $pdf;

    protected $serviceLocator;
    private $dbAdapter;

    protected $productoTable;
    protected $proveedorTable;
    protected $ventaTable;


    public function __construct(ServiceLocator $serviceLocator){

        $this->pdf = new \FPDF();

        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');  

        $this->productoTable = new \Almacen\Model\ProductoTable($this->dbAdapter);
        $this->proveedorTable = new \Almacen\Model\ProveedorTable($this->dbAdapter);
        $this->ventaTable = new \Ventas\Model\VentaTable($this->dbAdapter);

    }

    public  function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->pdf->GetStringWidth($txt);

        if ($str_width ==0) {
            $str_width = "1";
        }

        //echo $str_width."</br>";
 
        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->pdf->w-$this->pdf->rMargin-$this->pdf->x;
        $ratio = ($w-$this->pdf->cMargin*2)/$str_width;
 
        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->pdf->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->pdf->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->pdf->k;
                //Set character spacing
                $this->pdf->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }
 
        //Pass on to Cell method
        $this->pdf->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);
 
        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->pdf->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }
 
    public function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }
 
    //Patch to also work with CJK double-byte text
    public function MBGetStringLength($s)
    {
        if($this->pdf->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }




}