<?php

namespace RecursosHumano\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;


class EmpleadoController extends MainController
{
    public function indexAction()
    {
        $data = $this->getEmpleadoTable()->getAll();

        return new ViewModel(array(
            "empleado" => $data
            ));
    }

    public function addAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $data['fecha_nac'] = date("Y-m-d", strtotime($data['fecha_nac']));

            $this->getEmpleadoTable()->add($data->toArray());

            return $this->getResponse()->setContent("0");

        }
    	
    	$cargo = $this->getCargoTable()->getAllActive();

    	return new ViewModel(array(
    		"cargo" => $cargo
    		));
    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $data['fecha_nac'] = date("Y-m-d", strtotime($data['fecha_nac']));

            $res = $this->getEmpleadoTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $cargo = $this->getCargoTable()->getAllActive();

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $empleado = $this->getEmpleadoTable()->getById($id);

        return new ViewModel(array(
            "cargo" => $cargo,
            "empleado" => $empleado
            ));

    }

    public function imageUploadAction(){

        $dir = APPLICATION_PATH ."/img/usuarios";

        $imgSize = array("w"=>256 ,"h"=>256);

        $res = $this->imagenPlugin()->_uploadImage($dir,true,$imgSize);

        return $this->getResponse()->setContent($res);

    }

    public function searchByNameAction(){


        if ($this->getRequest()->isXmlHttpRequest()) {

            $name = $this->getRequest()->getPost();

            $data = $this->empleadoPlugin()->searchByName($name['nombre']);

          
            return $this->getResponse()->setContent($data);

        }

    }

 
}
