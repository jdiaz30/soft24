<?php

namespace RecursosHumano\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class EmpleadoPlugin extends AbstractPlugin {

	protected $empleadoTable;
    private $dbAdapter;
    protected $serviceLocator;

    public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');

        $this->empleadoTable = new \RecursosHumano\Model\EmpleadoTable($this->dbAdapter);
        
    }

    public function searchByName($name){
    	$htmlResponse = '';

    	$empData = $this->empleadoTable->searchByName($name);

    	foreach ($empData as $data) {

    		$htmlResponse .='<div class="col-lg-3 col-md-6">
			<div class="thumbnail">
			<div class="thumb thumb-rounded">';

			$foto = "/img/user.png";

			if ($data['foto'] !="" || $data['foto'] != null){

			  	$foto = "/img/usuarios/".$data['foto'];

			} 

			$htmlResponse .='<img src='.$foto.' alt="">';

			$htmlResponse .='<div class="caption-overflow">
				<span>
				<a href="/recursos-humano/empleado/edit/'.$data['id_empleado'].'" class="btn bg-success-400 btn-icon btn-xs"><i class="icon-link"></i></a>
				</span>
			</div>';

			$htmlResponse .='</div>';

			$htmlResponse .='<div class="caption text-center">';

			$htmlResponse .='<h6 class="text-semibold no-margin">'.$data['nombre'] ." ". $data['ap_pat'] ." ". $data['ap_mat'].' <small class="display-block">'.$data['descripcion'].'</small></h6>';

			$htmlResponse .='<ul class="icons-list mt-15">';

			if ($data['facebook'] !=null || $data['facebook'] !=""){

				$htmlResponse .='<li><a href="'.$data['facebook'].'" target="_blank" data-popup="tooltip" title="Facebook" data-container="body"><i class="icon-facebook"></i></a></li>';
			}

			if ($data['twitter'] !=null || $data['twitter'] !=""){

				$htmlResponse .='<li><a href="'.$data['twitter'].'" target="_blank" data-popup="tooltip" title="Twitter" data-container="body"><i class="icon-twitter"></i></a></li>';
			}

			if ($data['linkedin'] !=null || $data['linkedin'] !=""){

				$htmlResponse .='<li><a href="'.$data['linkedin'].'" target="_blank" data-popup="tooltip" title="Linkedin" data-container="body"><i class="icon-linkedin"></i></a></li>';
			}

			$htmlResponse .='</ul>';

			$htmlResponse .='</div>';//caption

			$htmlResponse .='</div></div>';

    	
    	}

    	return $htmlResponse;
    }

}