<?php

namespace RecursosHumano\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class EmpleadoTable extends AbstractTableGateway {

    protected $table = 'tb_empleado';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAll(){

    	$sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("e" => $this->table));

        $select->join(array("c"=>"tb_cargo"),"e.id_cargo = c.id_cargo",array("descripcion"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("e" => $this->table));

        $select->where(array("estado"=>"0"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getById($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from($this->table);

        $select->where(array("id_empleado" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        return $this->update($datos, array("id_empleado" => $datos['id_empleado']));
    }

    public function searchByName($nombre){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("e" => $this->table));

        $select->join(array("c"=>"tb_cargo"),"e.id_cargo = c.id_cargo",array("descripcion"));

        $select->where->like("e.nombre",$nombre."%");

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }


  


}

?>
