<?php

namespace Configuracion\View\Helper;

use Zend\View\Helper\AbstractHelper;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class PromocionHelper extends AbstractHelper {

    private $dbAdapter;
    protected $serviceLocator;
    protected $promoProdTable;


    public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter'); 

        $this->promoProdTable = new \Configuracion\Model\PromocionProdTable($this->dbAdapter);
    }

    public function __invoke($option,$data = null){
       $response = "";

       switch ($option) {
         case 'estado':

            $response = $this->verEstado($data);
           
           break;

          case 'productos':

            $response = $this->verProductos($data);
           
           break;

          case 'productos-edit':

            $response = $this->verProductosEdit($data);
           
           break;
         
         default:
           # code...
           break;
       }

       return $response;
   
    }

    public function verEstado($data){

        $estado = "";

        switch ($data) {
          case '0':
            $estado = '<span class="label label-success">Vigente</span>';
          break;
          case '1':
            $estado = '<span class="label label-danger">Finalizado</span>';
          break;

          default:
          # code...
          break;
        }

        return $estado;

    }

    public function verProductos($idPromo){

      $htmlResponse = '';

      $data = $this->promoProdTable->getAllByIdPromo($idPromo);

      $prod = array();

      foreach ($data as $productos) {
        
        $prod[] = '<span class="label label-info">'.$productos['nom_prod'].'</span>';
      }

      $htmlResponse = implode($prod, ",");

      return $htmlResponse;

    }

    public function verProductosEdit($idPromo){

       $data = $this->promoProdTable->getAllByIdPromo($idPromo);

       return $data;

    }





}

?>
