<?php

namespace Configuracion\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\File\Transfer\Adapter\Http;

use Zend\ServiceManager\ServiceLocatorInterface as ServiceLocator;

class UsuarioPlugin extends AbstractPlugin {

	protected $serviceLocator;
	private $dbAdapter;


    public function __construct(ServiceLocator $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->dbAdapter = $this->serviceLocator->get('Zend\Db\Adapter\Adapter');  
    }

    public function getModuleUser($idUser){

           return $idUser;
    }

}