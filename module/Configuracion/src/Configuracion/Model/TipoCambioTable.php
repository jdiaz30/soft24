<?php

namespace Configuracion\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class TipoCambioTable extends AbstractTableGateway {

    protected $table = 'tb_tipo_cambio';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);
    }

    public function getAll(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("tp" => $this->table));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }


    public function getById($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from($this->table);

        $select->where(array("id_tc" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function getByFecha($fecha){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from($this->table);

        $select->where(array("fecha" => $fecha));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_tc" => $datos['id_tc']));
    }


}

?>
