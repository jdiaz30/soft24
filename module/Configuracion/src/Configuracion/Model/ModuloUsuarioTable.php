<?php

namespace Configuracion\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class ModuloUsuarioTable extends AbstractTableGateway {

    protected $table = 'tb_modulo_usuario';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {

    	$modulo = explode(",", $datos['modulo']);

        foreach ($modulo as $data) {

            $dataModulo['id_modulo'] = $data;
            $dataModulo['id_usuario'] = $datos['id_usuario'];

           $this->insert($dataModulo);
        } 
    }

    public function getAllId($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();

        $select->from(array("mu"=>$this->table));

        $select->join(array("m"=>"tb_modulo"),"mu.id_modulo = m.id_modulo",array("modulo"=>"descripcion"));

        $select->where(array("id_usuario" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function remove($datos) {
        $this->delete(array("id_usuario" => $datos));
    }
}