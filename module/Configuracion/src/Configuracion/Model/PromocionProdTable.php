<?php

namespace Configuracion\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class PromocionProdTable extends AbstractTableGateway {

    protected $table = 'tb_promo_prod';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($producto , $idPromo) {

        foreach ($producto as $data) {
            $data['id_promo'] = $idPromo;
            $this->insert($data);
        }
    }

    public function getAllByIdPromo($idPromo){

        $sql = new Sql($this->adapter);
        $select = $sql->select();

        $select->from(array("pp" => $this->table));
        $select->join(array("pr"=>"tb_promocion"),"pp.id_promo = pr.id_promo" , array("nom_promo"));
        $select->join(array("p"=>"tb_producto"),"p.id_prod = pp.id_prod" , array("nom_prod","foto"));

        $select->where(array("pp.id_promo"=>$idPromo));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function edit($datos) {
        $this->update($datos, array("id_promo" => $datos['id_promo']));
    }

    public function remove($datos) {
        $this->delete(array("id_promo" => $datos));
    }

}

?>
