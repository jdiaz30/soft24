<?php


namespace Configuracion\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class SucursalController extends MainController
{
    public function indexAction()
    {
        $sucursal = $this->getSucursalTable()->getAll();

        return new ViewModel(array(
        	"sucursal"=>$sucursal
        	));
    }

    public function addAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

    		$this->getSucursalTable()->add($data->toArray());

    		return $this->getResponse()->setContent("0");

    	}

    }

    public function editAction(){


    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

    		$this->getSucursalTable()->edit($data->toArray());

    		return $this->getResponse()->setContent("0");

    	}

    	$id = $this->getEvent()->getRouteMatch()->getParam('id');

    	$sucursal = $this->getSucursalTable()->getById($id);

    	return new ViewModel(array(
    		"sucursal" => $sucursal
    		));

    }

}
