<?php


namespace Configuracion\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class PromocionController extends MainController
{

	protected $usuarioSession;

	public function __construct(){

      $this->usuarioSession = $this->getUserSession();     
  	}

    public function indexAction(){

    	$promocion = $this->getPromocionTable()->getAll();

    	return  new ViewModel(array(
    		"promocion" => $promocion
    		));
    }

    public function addAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

    		$data['fec_ini'] = date("Y-m-d", strtotime($data['fec_ini']));
            $data['fec_fin'] = date("Y-m-d", strtotime($data['fec_fin']));
            
			$data['estado'] = "0";
			$data['id_usuario'] = $this->usuarioSession->id;

			$productos = json_decode($data['productos'],true);

            unset($data['tb_prod_promo_length']);
            unset($data['productos']);

    		$idPromo = $this->getPromocionTable()->add($data->toArray());

    		$this->getPromoProdTable()->add($productos,$idPromo);

    		return $this->getResponse()->setContent("0");

    	}

       	$producto = $this->getProductoTable()->getAllByVentas();

       	return new ViewModel(array(
            "productos" => $producto,
        ));
    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $data['fec_ini'] = date("Y-m-d", strtotime($data['fec_ini']));
            $data['fec_fin'] = date("Y-m-d", strtotime($data['fec_fin']));
            $data['estado'] = "0";
            $data['id_usuario'] = $this->usuarioSession->id;

            $productos = json_decode($data['productos'],true);

            //unset($data['fec_ini_submit']);
           // unset($data['fec_fin_submit']);
            unset($data['tb_prod_promo_length']);
            unset($data['productos']);
         
            echo var_dump($data);

            $this->getPromocionTable()->edit($data->toArray());

            $this->getPromoProdTable()->remove($data['id_promo']);

            $this->getPromoProdTable()->add($productos,$data['id_promo']);

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $promocion = $this->getPromocionTable()->getById($id);

        $producto = $this->getProductoTable()->getAllByVentas();

        return new ViewModel(array(
            "promocion" => $promocion,
            "productos" => $producto

            ));
    }

   

  

}
