<?php


namespace Configuracion\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class CargoController extends MainController
{
    public function indexAction()
    {
      $cargo = $this->getCargoTable()->getAll();

      return new ViewModel(array(
        "cargo"=>$cargo
        ));

    }

    public function addAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getCargoTable()->add($data->toArray());

            return $this->getResponse()->setContent("0");

        }

    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getCargoTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $cargo= $this->getCargoTable()->getById($id);

        return new ViewModel(array(
            "cargo" => $cargo
            ));
    }

}
