<?php


namespace Configuracion\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class TipoCambioController extends MainController
{

    protected $usuarioSession;

    public function __construct(){

      $this->usuarioSession = $this->getUserSession();

        
    }
    public function indexAction()
    {
        $data = $this->getTipoCambioTable()->getAll();

        return new ViewModel(array(
            "tipoCambio" =>$data
            ));
    }

    public function addAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $data['id_usuario'] = $this->usuarioSession->id;

            $this->getTipoCambioTable()->add($data->toArray());

            return $this->getResponse()->setContent("0");

        }

    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $data['id_usuario'] = $this->usuarioSession->id;

            $this->getTipoCambioTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $tCambio = $this->getTipoCambioTable()->getById($id);

        return new ViewModel(array(
            "tipoCambio" => $tCambio
            ));

    }

  


  

}
