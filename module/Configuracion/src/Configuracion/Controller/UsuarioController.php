<?php


namespace Configuracion\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class UsuarioController extends MainController
{
    public function indexAction()
    {

        $usuario = $this->getUsuarioTable()->getAll();

        return new ViewModel(array(
            "usuario" => $usuario
            ));
      
    }

    public function addAction(){

    	if ($this->getRequest()->isXmlHttpRequest()) {

    		$data = $this->getRequest()->getPost();

    		$data['user_pass'] = password_hash($data['user_pass'], PASSWORD_BCRYPT);

    		$idUsuario = $this->getUsuarioTable()->add($data->toArray());

    		return $this->getResponse()->setContent("0");

    	}

    	$empleado = $this->getEmpleadoTable()->getAllActive();

    	return  new ViewModel(array(
    		"empleado" => $empleado
    		));
    	
    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            if ($data['user_pass'] != $data['user_pass_o']) {

                $data['user_pass'] = password_hash($data['user_pass'], PASSWORD_BCRYPT);
               
            } 

            unset($data['user_pass_o']);

            $this->getUsuarioTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $usuario = $this->getUsuarioTable()->getAllId($id);

        $empleado = $this->getEmpleadoTable()->getAllActive();

        return  new ViewModel(array(

            "empleado" => $empleado,
            "usuario" => $usuario,

            ));

    }


}
