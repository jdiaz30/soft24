<?php


namespace Configuracion\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class CajaController extends MainController
{
    public function indexAction()
    {
        $caja = $this->getCajaTable()->getAll();

        return new ViewModel(array(
            "caja" => $caja
        ));

    }

    public function addAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getCajaTable()->add($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $sucursal = $this->getSucursalTable()->getAllActive();

        return new ViewModel(array(
            "sucursal" => $sucursal
        ));

    }

    public function editAction(){

        if ($this->getRequest()->isXmlHttpRequest()) {

            $data = $this->getRequest()->getPost();

            $this->getCajaTable()->edit($data->toArray());

            return $this->getResponse()->setContent("0");

        }

        $id = $this->getEvent()->getRouteMatch()->getParam('id');

        $caja = $this->getCajaTable()->getById($id);

        $sucursal = $this->getSucursalTable()->getAllActive();

        return new ViewModel(array(
            "sucursal" => $sucursal,
            "caja" => $caja
        ));

    }


  

}
