<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonModule for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Configuracion;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Configuracion\Model\SucursalTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Configuracion\Model\SucursalTable($dbAdapter);

                    return $table;
                },
                'Configuracion\Model\CargoTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Configuracion\Model\CargoTable($dbAdapter);

                    return $table;
                },

                'Configuracion\Model\CajaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Configuracion\Model\CajaTable($dbAdapter);

                    return $table;
                },

                'Configuracion\Model\PromocionTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Configuracion\Model\PromocionTable($dbAdapter);

                    return $table;
                },
                'Configuracion\Model\PromocionProdTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Configuracion\Model\PromocionProdTable($dbAdapter);

                    return $table;
                },
                'Configuracion\Model\TipoCambioTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Configuracion\Model\TipoCambioTable($dbAdapter);

                    return $table;
                },
         
                )
            );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
      
                'usuarioHelper' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Configuracion\View\Helper\UsuarioHelper($serviceLocator);
                },
                'promocionHelper' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Configuracion\View\Helper\PromocionHelper($serviceLocator);
             
                }
            ),
        );
    }

    public function getControllerPluginConfig(){
        return array(
            'factories' => array(
                'usuarioPlugin' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Configuracion\Plugin\UsuarioPlugin($serviceLocator);

                }
              
                ),
            );
    }

}
