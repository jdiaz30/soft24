<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Configuracion\Controller\Index' => 'Configuracion\Controller\IndexController',
            'Configuracion\Controller\Sucursal' => 'Configuracion\Controller\SucursalController',
            'Configuracion\Controller\Cargo' => 'Configuracion\Controller\CargoController',
            'Configuracion\Controller\Usuario' => 'Configuracion\Controller\UsuarioController',  
            'Configuracion\Controller\Caja' => 'Configuracion\Controller\CajaController', 
            'Configuracion\Controller\Promocion' => 'Configuracion\Controller\PromocionController', 
            'Configuracion\Controller\TipoCambio' => 'Configuracion\Controller\TipoCambioController', 

        ),
    ),
    'router' => array(
        'routes' => array(
            'configuracion' => array(
                'type'    => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route'    => '/configuracion',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Configuracion\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]][/:id][/]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Configuracion' => __DIR__ . '/../view',
        ),
    ),
);
