<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Controller\Main\MainController;
use Zend\Session\Container;

class LoginController extends MainController
{

  protected $usuarioSession;

  public function __construct(){

      $this->usuarioSession = $this->getUserSession();     
  }


  public function indexAction(){
    $this->layout('login');

    $authService = $this->getAuthService();
	  //Si el usuario ya inicio sesion, redirige a la home
    if ($authService->hasIdentity()) {

      return $this->redirect()->toUrl('/');
    }
    // Cerramos cualquier sesion que pueda quedar
    $authService->clearIdentity();

    // Si el usuario ha enviado el formulario
    if ($this->getRequest()->isXmlHttpRequest()) {
      // Preparamos el adaptador auth
      $authAdapter = $authService->getAdapter();
      $authAdapter->setIdentity($this->getRequest()->getPost('user_name'))
      ->setCredential($this->getRequest()->getPost('user_pass'));

      // Intentamos autentificar el usuario
      $result = $authAdapter->authenticate();

      if ($result->isValid()) {
        //Registramos la sesión
        $storage = $this->getSessionStorage();
        $storage->write($authAdapter->getResultRowObject(null, 'password'));

        //Guardamos los datos de las session
        $usuarioData = $this->getUsuarioTable()->getUserName($this->getRequest()->getPost('user_name'));

        $this->usuarioSession->id = $usuarioData['id_usuario'];
        $this->usuarioSession->nombre = $usuarioData['nom_emp'];
        $this->usuarioSession->cargo= $usuarioData['cargo'];
        $this->usuarioSession->foto = $usuarioData['foto'];

     
        $ipUsuario = $this->accesoSistemaPlugin()->getRealIp();

        $accesoData['id_usuario'] = $this->usuarioSession->id;
        $accesoData['fecha'] = date("Y/m/d H:i:s");
        $accesoData['ip'] = $ipUsuario;
        $accesoData['tipo_acceso'] = "i";//ingreso
        $accesoData['estado'] = "0";

        $this->getAccesoSistemaTable()->add($accesoData);

        return $this->getResponse()->setContent("0");
      } else {
        return $this->getResponse()->setContent("1");
      }
    }
  }

  public function logoutAction() {
    $this->getSessionStorage()->clear();
    $this->getAuthService()->clearIdentity();

    $ipUsuario = $this->accesoSistemaPlugin()->getRealIp();

    $accesoData['id_usuario'] = $this->usuarioSession->id;
    $accesoData['fecha'] = date("Y/m/d H:i:s");
    $accesoData['ip'] = $ipUsuario;
    $accesoData['tipo_acceso'] = "s";//salida
    $accesoData['estado'] = "0";

    $idAcceso = $this->getAccesoSistemaTable()->add($accesoData);

    $this->usuarioSession->exchangeArray(array());

    return $this->redirect()->toUrl('/');
  }



}
