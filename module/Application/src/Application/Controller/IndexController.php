<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Controller\Main\MainController;
use Zend\View\Model\ViewModel;

class IndexController extends MainController
{
    public function indexAction()
    {

    	$auth = new \Zend\Authentication\AuthenticationService();
        
     	$identi = $auth->getStorage()->read();

        //echo var_dump($cajaSession->apertura);

     	//$cajaSession->exchangeArray(array());

     	if ($identi->rol == "cajero") {

     		$caja = $this->movCajaPlugin()->getCajaDisponible();

     		return new ViewModel(array(
     			"apertura" =>"si" ,
     			"caja" => $caja
     			));
     	}

       	
    }
}
