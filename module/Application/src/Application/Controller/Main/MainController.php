<?php

namespace Application\Controller\Main;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Authentication\AuthenticationService;


class MainController extends AbstractActionController
{

    protected $storage;
    protected $authservice;
    
    protected $usuarioTable;
    protected $familiaProductoTable;
    protected $tipoProductoTable;
    protected $productoTable;
    protected $proveedorTable;
    protected $provProdTable;
    protected $sucursalTable;
    protected $almacenTable;
    protected $cargoTable;
    protected $empleadoTable;

    protected $compraTable;
    protected $compraProdTable;
    protected $sessionUser;
    protected $cajaTable;
    protected $clienteTable;
    protected $ventaTable;
    protected $ventaProductoTable;
    protected $promocionTable;
    protected $promoProdTable;
    protected $movCajaTable;
    protected $accesoSistemaTable;
    protected $tipoCambioTable;

    public function getUserSession(){

        $this->sessionUser = new \Zend\Session\Container("usuario");

        return $this->sessionUser;

    }

    public function getTipoCambioTable() {
        if (!$this->tipoCambioTable) {
            $sm = $this->getServiceLocator();
            $this->tipoCambioTable = $sm->get('Configuracion\Model\TipoCambioTable');
        }
        return $this->tipoCambioTable;
    }

    public function getAccesoSistemaTable() {
        if (!$this->accesoSistemaTable) {
            $sm = $this->getServiceLocator();
            $this->accesoSistemaTable = $sm->get('Application\Model\AccesoSistemaTable');
        }
        return $this->accesoSistemaTable;
    }

    public function getMovCajaTable() {
        if (!$this->movCajaTable) {
            $sm = $this->getServiceLocator();
            $this->movCajaTable = $sm->get('Ventas\Model\MovimientoCajaTable');
        }
        return $this->movCajaTable;
    }

    public function getPromocionTable() {
        if (!$this->promocionTable) {
            $sm = $this->getServiceLocator();
            $this->promocionTable = $sm->get('Configuracion\Model\PromocionTable');
        }
        return $this->promocionTable;
    }

    public function getPromoProdTable() {
        if (!$this->promoProdTable) {
            $sm = $this->getServiceLocator();
            $this->promoProdTable = $sm->get('Configuracion\Model\PromocionProdTable');
        }
        return $this->promoProdTable;
    }


    public function getVentaTable() {
        if (!$this->ventaTable) {
            $sm = $this->getServiceLocator();
            $this->ventaTable = $sm->get('Ventas\Model\VentaTable');
        }
        return $this->ventaTable;
    }

    public function getVentaProductoTable() {
        if (!$this->ventaProductoTable) {
            $sm = $this->getServiceLocator();
            $this->ventaProductoTable = $sm->get('Ventas\Model\VentaProductoTable');
        }
        return $this->ventaProductoTable;
    }

    public function getClienteTable() {
        if (!$this->clienteTable) {
            $sm = $this->getServiceLocator();
            $this->clienteTable = $sm->get('Ventas\Model\ClienteTable');
        }
        return $this->clienteTable;
    }

    public function getCajaTable() {
        if (!$this->cajaTable) {
            $sm = $this->getServiceLocator();
            $this->cajaTable = $sm->get('Configuracion\Model\CajaTable');
        }
        return $this->cajaTable;
    }


    public function getCompraProductoTable() {
        if (!$this->compraProdTable) {
            $sm = $this->getServiceLocator();
            $this->compraProdTable = $sm->get('Compras\Model\CompraProductoTable');
        }
        return $this->compraProdTable;
    }

    public function getComprasTable() {
        if (!$this->compraTable) {
            $sm = $this->getServiceLocator();
            $this->compraTable = $sm->get('Compras\Model\CompraTable');
        }
        return $this->compraTable;
    }



    public function getEmpleadoTable() {
        if (!$this->empleadoTable) {
            $sm = $this->getServiceLocator();
            $this->empleadoTable = $sm->get('RecursosHumano\Model\EmpleadoTable');
        }
        return $this->empleadoTable;
    }

    public function getCargoTable() {
        if (!$this->cargoTable) {
            $sm = $this->getServiceLocator();
            $this->cargoTable = $sm->get('Configuracion\Model\CargoTable');
        }
        return $this->cargoTable;
    }

    public function getAlmacenTable() {
        if (!$this->almacenTable) {
            $sm = $this->getServiceLocator();
            $this->almacenTable = $sm->get('Almacen\Model\AlmacenTable');
        }
        return $this->almacenTable;
    }

    public function getSucursalTable() {
        if (!$this->sucursalTable) {
            $sm = $this->getServiceLocator();
            $this->sucursalTable = $sm->get('Configuracion\Model\SucursalTable');
        }
        return $this->sucursalTable;
    }

    public function getProvProductoTable() {
        if (!$this->provProdTable) {
            $sm = $this->getServiceLocator();
            $this->provProdTable = $sm->get('Almacen\Model\ProvProductoTable');
        }
        return $this->provProdTable;
    }

    public function getFamiliaProductoTable() {
        if (!$this->familiaProductoTable) {
            $sm = $this->getServiceLocator();
            $this->familiaProductoTable = $sm->get('Almacen\Model\FamiliaProductoTable');
        }
        return $this->familiaProductoTable;
    }

    public function getProveedorTable() {
        if (!$this->proveedorTable) {
            $sm = $this->getServiceLocator();
            $this->proveedorTable = $sm->get('Almacen\Model\ProveedorTable');
        }
        return $this->proveedorTable;
    }

    public function getTipoProductoTable() {
        if (!$this->tipoProductoTable) {
            $sm = $this->getServiceLocator();
            $this->tipoProductoTable = $sm->get('Almacen\Model\TipoProductoTable');
        }
        return $this->tipoProductoTable;
    }

    public function getProductoTable() {
        if (!$this->productoTable) {
            $sm = $this->getServiceLocator();
            $this->productoTable = $sm->get('Almacen\Model\ProductoTable');
        }
        return $this->productoTable;
    }


    public function getUsuarioTable() {
        if (!$this->usuarioTable) {
            $sm = $this->getServiceLocator();
            $this->usuarioTable = $sm->get('Application\Model\UsuarioTable');
        }
        return $this->usuarioTable;
    }

    public function getAuthService() {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()->get('AuthService');
        }
        return $this->authservice;
    }

    public function getSessionStorage() {
        if (!$this->storage) {
            $auth = new AuthenticationService();
            $this->storage = $auth->getStorage();
        }
        return $this->storage;
    }
}
