<?php

namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class UsuarioTable extends AbstractTableGateway {

    protected $table = 'tb_usuario';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);

        return $this->lastInsertValue;
    }

    public function getUserName($userName){

        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from(array("u"=>$this->table));

        $select->join(array("e"=>"tb_empleado"),"u.id_empleado = e.id_empleado",array("nom_emp"=>"nombre","ap_pat","ap_mat","foto"));
        $select->join(array("c"=>"tb_cargo"),"c.id_cargo = e.id_cargo",array("cargo"=>"descripcion"));

        $select->where(array("user_name" => $userName));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function getAll(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("u" => $this->table));

        $select->join(array("e"=>"tb_empleado"),"u.id_empleado = e.id_empleado",array("nom_emp"=>"nombre","ap_pat","ap_mat"));
        $select->join(array("c"=>"tb_cargo"),"c.id_cargo = e.id_cargo",array("cargo"=>"descripcion"));

        $select->quantifier('DISTINCT');

        $select->order(array("u.id_usuario"));

        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;
    }

    public function getAllActive(){

        $sql = new Sql($this->adapter);
        $select = $sql->select();
        $select->from(array("u" => $this->table));
        $select->where(array("estado" =>"0"));
        // $select->columns(array("*"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        //Mostramos todos los registros
        $resultSet = new ResultSet;
        $resultSet->initialize($result);

        return $resultSet;

    }
    public function getAllId($id){
        $sql = new Sql($this->adapter);

        $select = $sql->select();
        $select->from($this->table);

        $select->where(array("id_usuario" => $id));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $row = $result->current(); //Mostramos solo un registro

        return $row;
    }

    public function edit($datos) {
        $this->update($datos, array("id_usuario" => $datos['id_usuario']));
    }
   

}

?>
