<?php

namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class AccesoSistemaTable extends AbstractTableGateway {

    protected $table = 'tb_acceso_sistema';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function add($datos) {
        $this->insert($datos);

        return $this->lastInsertValue;
    }

    public function edit($datos) {
        $this->update($datos, array("id_acceso" => $datos['id_acceso']));
    }

  


}

?>
