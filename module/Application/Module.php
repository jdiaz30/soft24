<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $authListener = new \Application\Authentication\AuthenticationListener();
        $authListener->attach($eventManager);

        $this->initAcl($e);

        $e->getApplication()->getEventManager()->attach('route', array($this, 'checkAcl'));

        date_default_timezone_set('America/Lima');

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Application\Model\UsuarioTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Application\Model\UsuarioTable($dbAdapter);

                    return $table;
                },
                'Application\Model\AccesoSistemaTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new \Application\Model\AccesoSistemaTable($dbAdapter);

                    return $table;
                }

                )
            );
    }

    public function initAcl(MvcEvent $e){

        //Creamos el objeto ACL

        $acl = new \Zend\Permissions\Acl\Acl();

        //Incluimos la lista de roles y permisos, nos devuelve un array

        $roles = require_once  APPLICATION_PATH . '/../config/autoload/acl.roles.php';

        foreach($roles as $role => $resources){

            //Indicamos que el rol será genérico

            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);

            //Añadimos el rol al ACL
            $acl->addRole($role);

            //Recorremos los recursos o rutas permitidas
            foreach($resources["allow"] as $resource){

                //Si el recurso no existe lo añadimos
                 if(!$acl->hasResource($resource)){
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                 }
                  
                 //Permitimos a ese rol ese recurso
                 $acl->allow($role, $resource);

            }
             
            foreach ($resources["deny"] as $resource) {
                  
                 //Si el recurso no existe lo añadimos
                 if(!$acl->hasResource($resource)){
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                 }
                  
                 //Denegamos a ese rol ese recurso
                 $acl->deny($role, $resource);
            }
        }
         

        //Establecemos la lista de control de acceso

        $e->getViewModel()->acl=$acl;

    }

    public function checkAcl(MvcEvent $e){

        //guardamos el nombre de la ruta o recurso a permitir o denegar

        $route = $e->getRouteMatch()->getMatchedRouteName();

        $pos = strpos($route, "/");

        $route = substr($route, 0,$pos);

        //Instanciamos el servicio de autenticacion

        $auth = new \Zend\Authentication\AuthenticationService();

        $identi=$auth->getStorage()->read();

        // Establecemos nuestro rol

        // $userRole = 'admin';

        // Si el usuario esta identificado le asignaremos el rol admin y si no el rol visitante.

        if($identi!=false && $identi!=null){

           $userRole = $identi->rol;

        }else{

           $userRole = "visitante";

        }

        //Comprobamos si no está permitido para ese rol esa ruta

        if(!$e->getViewModel()->acl->isAllowed($userRole, $route)) {

        //Devolvemos un error 404

            $response = $e->getResponse();

            $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');

            $response->setStatusCode(404);

        }

    }

    public function getControllerPluginConfig(){
        return array(
            'factories' => array(
                'accesoSistemaPlugin' => function($serviceManager) {
                    $serviceLocator = $serviceManager->getServiceLocator();
                    // pass it to your helper 
                    return new \Application\Plugin\AccesoSistemaPlugin($serviceLocator);

                }
              
                ),
            );
    }

}



