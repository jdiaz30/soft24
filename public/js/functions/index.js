var stack_top_left = {"dir1": "down", "dir2": "right", "push": "top"};
var stack_bottom_left = {"dir1": "right", "dir2": "up", "push": "top"};
var stack_bottom_right = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};
var stack_custom_left = {"dir1": "right", "dir2": "down"};
var stack_custom_right = {"dir1": "left", "dir2": "up", "push": "top"};
var stack_custom_top = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 1};
var stack_custom_bottom = {"dir1": "up", "dir2": "right", "spacing1": 1};

function show_stack_custom_right(type,title,text) {
    var opts = {
        title: "Over here",
        text: "Check me out. I'm in a different stack.",
        addclass: "stack-custom-right bg-primary",
       // stack: stack_custom_right
    };
    switch (type) {
        case 'error':
        opts.title = title;
        opts.text = text;
        opts.addclass = "bg-danger";
        //opts.type = "error";
        break;

        case 'info':
        opts.title = title;
        opts.text = text;
        opts.addclass = "bg-info";
       // opts.type = "info";
        break;

        case 'success':
        opts.title = title;
        opts.text = text;
        opts.addclass = "bg-success";
       // opts.type = "success";
        break;
    }
    new PNotify(opts);
}

function init_validate_form(form){
    var validate = $(form).validate({
        ignore: 'input[type=hidden], .select2-input', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {

           $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                 else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons
            else if (element.parents('div').hasClass('has-feedback')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }
            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function(label) {
            //label.removeClass("validation-error-label");
            label.addClass("validation-valid-label").text("Datos ingresados.");
        },
        rules: {
            vali: "required",
            password: {
                minlength: 5
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            card: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        }
    });

    return validate;

}

function nobackbutton(){
 
   window.location.hash="no-back-button";

   window.location.hash="Again-No-back-button"; //chrome

   window.onhashchange=function(){window.location.hash="";};
  
}

function decimalesTienda(vNumber){

    var vIndex = vNumber.indexOf(".");
    var vEntero = vNumber.substr(0,vIndex);
    var vDecimal = vNumber.substr(vIndex +1);

    //console.log(vDecimal);

    var vDecimal1 = parseInt(vDecimal.substr(0,1));

    vDecimal = parseInt(vDecimal.substr(1));

    if (vDecimal >=5) {
        vDecimal1 += 1;
        vDecimal = 0;
    }else{
        vDecimal = 0;
    }

    var vFinal = vDecimal1+""+vDecimal;

    vFinal = vEntero +"."+vFinal;

    return vFinal;

}

function getSessionCaja(){
    return JSON.parse(localStorage.getItem("caja") || "{}");
}

function deleteSessionCaja(){
    localStorage.removeItem('caja');
}
