$(function() {

	var vTableVentas = null;
	var vProductos = [];
  var vTipoDoc = "boleta";
  var vCajaSession = getSessionCaja();
	var vVentas = {
		init : function(){
			
          vTableVentas = configTable("#tb_producto");

          configTable("#tb_producto_modal");

          $("#mont_ing").maskMoney({prefix: 'S/. '});

          $("#btn-activar-lectora").click(function(){

          	vVentas.activarLectora();

          });

          $("#prod_lectora").keypress(function(e){

          	if(e.which == 13) {
          		vVentas.getProductoByBarcCode($(this).val());
          	}

          });

          $("#mont_ing").keyup(function(){

          	vVentas.calculaVuelto();

          });

          $(".btn-cliente").click(function(){

          	vVentas.registrarCliente();

          });

          $("#ruc_cliente").change(function(){

          	vVentas.selectCliente();

          });

          $("#moneda").change(function(){

            $("#vuelto").get(0).value = "0.00";
            $("#mont_ing").get(0).value = "0.00";

            if ($("#moneda option:selected").val() == "dolares") {

              $("#mont_ing").maskMoney({prefix: '$ '});

            }else{

              $("#mont_ing").maskMoney({prefix: 'S/. '});

            }

            vVentas.calculaTotalPagar();

          });

          $("#dni_cliente").change(function(){

            vVentas.selectCliente();

          });

          $(".btn-pago").click(function(){

            vVentas.validaRegistrar();

          });

          $("#btn-n-venta").click(function(){

            if ($("#btn-n-venta").hasClass("disabled") === false) {
               location.reload();
            }
               
          });

          $("#btn-caja-si").click(function(){

            vVentas.cerrarCaja();

          });

          vVentas.verAperturaCaja();

          vVentas.addProductoManual(); 
          vVentas.selectFormaPago();
          vVentas.deleteProducto();
          vVentas.tipoDocSelect();
          vVentas.atajosTeclado();
          vVentas.activarLectora();

          //alert(JSON.stringify(vCajaSession));

          setInterval(function(){

          	vVentas.verHora();

          }, 1000);
		},
		activarLectora : function(){

			$("#prod_lectora").focus();
		},
		addProductoManual : function(){
			$('#tb_producto_modal tbody').on( 'click', '.dropdown-menu.dropdown-menu-right li a', function () {

				        var vIdProd = parseInt($(this).parents('tr').find("td:first").html());
                var vFoto = $(this).parents('tr').find("td:eq(2)").html();
                var vProd = vFoto +" </br>"+ $(this).parents('tr').find("td:eq(3)").html() +"<input type='hidden' value="+vIdProd+">";
                var vPrecio = $(this).parents('tr').find("td:eq(4)").html();
                var vCantidad = 1;
                var vCantTabla = '<input type="text" value = "'+vCantidad+'" class="form-control qty">'; 

                var vPrecioTabla = '<input type="text" value = "'+vPrecio+'" class="form-control money">'; 
                var vImporte = parseFloat(vPrecio.replace(",","")) * vCantidad;
                var vCodBarras = $(this).parents('tr').find("td:eq(1)").html();

                var vDelete = ' <button type="button" class="btn btn-danger btn-float btn-delete" data-popup="tooltip" title="Eliminar producto" data-original-title="Eliminar producto" data-placement="bottom"><i class="icon-cart-remove"></i></button>';
              
                var vItem = vProductos.length + 1;

                if (vProductos.indexOf(vIdProd) == -1) {

                	vTableVentas.row.add( [vItem,vCodBarras,vProd,vPrecioTabla,vCantTabla,vImporte.toFixed(2),vDelete]).draw();

                	vProductos.push(vIdProd);
                	vVentas.changeQtyProducto(); 
                  vVentas.changePrecioProducto(); 

                  $(".money").maskMoney();

                }else{

                	$("#tb_producto tbody tr").each(function (index) {

                		if ($(this).find("td:eq(2)").find("input").val() == vIdProd) {

                			vCantidad = parseInt($(this).find("td:eq(4)").find("input").val()) + 1; 
                			vCantTabla = '<input type="text" value = "'+vCantidad+'" class="form-control">'; 

                			vImporte = parseFloat(vPrecio.replace(",","")) * vCantidad;

                			$(this).find("td:eq(4)").html(vCantTabla); 
                			$(this).find("td:eq(5)").html(vImporte.toFixed(2)); 

                		}

                	});

                }

           
                vVentas.totalProductos();

				        vVentas.calculaTotalPagar();

			});
		},
    totalProductos : function(){
      var vTotalArt = vVentas.getProductosTabla();

      $("#cant_art").val(vTotalArt.length);
    },
		getProductoByBarcCode : function(vBarCode){

			$.ajax({
                url:"/ventas/ventas/get-producto-by-bar-code",  
                type:'POST',
                dataType :"json",
                async:true, 
                data: {
                	cod_barras : vBarCode
                },
                beforeSend:function(){
                 
                },
                success:function(respuesta){

                   $("#prod_lectora").val("");
                   $("#prod_lectora").focus();

                   vVentas.addProductoByBarCode(respuesta);
           
                }
            });
		},
		addProductoByBarCode : function(vData){

			var vIdProd = vData.id_prod;
			var vFoto = "/img/thumbnail-default.jpg";
			var vCantidad = 1;
			var vCantTabla = '<input type="text" value = "'+vCantidad+'" class="form-control qty">';
			var vPrecio = vData.prec_vent;
      var vPrecioTabla = '<input type="text" value = "'+vPrecio+'" class="form-control money">'; 
			var vImporte = parseFloat(vPrecio.replace(",","")) * vCantidad;
			var vCodBarras = vData.cod_barras;
			var vItem = vProductos.length + 1;

      var vDelete = ' <button type="button" class="btn btn-danger btn-float btn-delete" data-popup="tooltip" title="Eliminar producto" data-original-title="Eliminar producto" data-placement="bottom"><i class="icon-cart-remove"></i></button>';
              
			if (vData.foto !== ""){
				vFoto = "/img/productos/"+ vData.foto;
			}

			vFoto = '<img src="'+vFoto+'" width="100">';

			var vProducto = vFoto +" </br>"+ vData.nom_prod +" / "+vData.nom_tipo +"<input type='hidden' value="+vIdProd+">" ;

			if (vProductos.indexOf(vIdProd) == -1) {

				vTableVentas.row.add( [vItem,vCodBarras,vProducto,vPrecioTabla,vCantTabla,vImporte.toFixed(2),vDelete]).draw();

				vProductos.push(vIdProd);

				vVentas.changeQtyProducto(); 
        vVentas.changePrecioProducto(); 

        $(".money").maskMoney();


			}else{

				$("#tb_producto tbody tr").each(function (index) {

					if ($(this).find("td:eq(2)").find("input").val() == vIdProd) {

						vCantidad = parseInt($(this).find("td:eq(4)").find("input").val()) + 1; 
						vCantTabla = '<input type="text" value = "'+vCantidad+'" class="form-control">'; 

						vImporte = parseFloat(vPrecio.replace(",","")) * vCantidad;

						$(this).find("td:eq(4)").html(vCantTabla); 
						$(this).find("td:eq(5)").html(vImporte.toFixed(2)); 

					}

				});

			}

      vVentas.totalProductos();

			vVentas.calculaTotalPagar();
		},
		changeQtyProducto : function(){

			$(".qty").keyup(function(){

				var vPrecio = $(this).parents('tr').find("td:eq(3) input").val();

				var vCantidad = parseInt($(this).val());

				var vImporte = parseFloat(vPrecio.replace(",","")) * vCantidad;

				$(this).parents('tr').find("td:eq(5)").html(vImporte.toFixed(2));


				vVentas.calculaTotalPagar();

			});
		},
    changePrecioProducto : function(){

      $(".money").keyup(function(){

        var vCantidad = parseInt($(this).parents('tr').find("td:eq(4) input").val());

        var vPrecio = $(this).val();

        vPrecio = vPrecio.replace(",","");

        var vImporte = vPrecio* vCantidad;

        $(this).parents('tr').find("td:eq(5)").html(vImporte.toFixed(2));


        vVentas.calculaTotalPagar();

      });
    },
		calculaTotalPagar : function(){

			var vSubTotal = 0;
			var vDscto = parseFloat($(".dscto").html());
			var vIgvCalc = parseFloat($(".igv").html());
			var vTotal = 0;
      var vTipoCambio = $("#tipo_cambio").val();

			var vIgv = 0.18;
			var vSubTotalSinIgv = 0;

			$("#tb_producto tbody tr").each(function (index) {

        if ($(this).find("td:eq(0)").html() != "No data available in table" ) {

          var vImporte = parseFloat($(this).find("td:eq(5)").html());
          vSubTotal += vImporte;

        }

				//vTotal = (vSubTotal - vDscto) + vIgv;
			});

      if (vSubTotal !== 0) {

        vSubTotalSinIgv = vSubTotal / 1.18;

        vIgvCalc = vSubTotalSinIgv * vIgv ;

        vTotal = vSubTotalSinIgv + vIgvCalc;

        $(".sub-total").html(vSubTotalSinIgv.toFixed(2));
        $(".igv").html(vIgvCalc.toFixed(2));
        $(".total").html(vTotal.toFixed(2));

        if ($("#moneda option:selected").val() == "dolares") {

          vTotal = vTipoCambio * vTotal;

          $("#m-pagar").val("$ " + vTotal.toFixed(2));

        }else{

          $("#m-pagar").val("S/. " + vTotal.toFixed(2));

        }

      }else{

        $(".sub-total").html("0.00");
        $(".igv").html("0.00");
        $(".total").html("0.00");

        $("#m-pagar").val("0.00");

      }
		},
		selectFormaPago : function(){

			$("#f_pago").change(function(){

				var vIndex = $("#f_pago").get(0).selectedIndex;

				if (vIndex == 2) {

					$("#pago-tarjeta-select").fadeIn(100);

				}else{
					$("#pago-tarjeta-select").fadeOut(100);
				}

			});
		},
		calculaVuelto : function(){

			var vTotal = parseFloat($(".total").html());
			var vRecibi = $("#mont_ing").val();
      var vVuelto = 0;
      var vTipoCambio = $("#tipo_cambio").val();

      if ($("#moneda option:selected").val() == "dolares") {

        vRecibi = vRecibi.replace("$ ","");
        vRecibi = vRecibi.replace(",","");

        vVuelto = (vRecibi - (vTotal * vTipoCambio )) * vTipoCambio;

        $("#vuelto").get(0).value = "S/. " + vVuelto.toFixed(2);

        $("#vuelto").maskMoney('mask');

        console.log("dolares" + vTotal);

      }else{

        vRecibi = vRecibi.replace("S/. ","");
        vRecibi = vRecibi.replace(",","");

        vVuelto = vRecibi - vTotal;

        $("#vuelto").get(0).value = "S/. " + vVuelto.toFixed(2);
        $("#vuelto").maskMoney('mask');

      }

			
		},
		verHora : function(){

			var crTime = new Date ( );
			var crHrs = crTime.getHours ( );
			var crMns = crTime.getMinutes ( );
			var crScs = crTime.getSeconds ( );
			crMns = ( crMns < 10 ? "0" : "" ) + crMns;
			crScs = ( crScs < 10 ? "0" : "" ) + crScs;
			var timeOfDay = ( crHrs < 12 ) ? "AM" : "PM";
			crHrs = ( crHrs > 12 ) ? crHrs - 12 : crHrs;
			crHrs = ( crHrs === 0 ) ? 12 : crHrs;
			var crTimeString = crHrs + ":" + crMns + ":" + crScs + " " + timeOfDay;

			$("#hora").val(crTimeString);		
		},
		registrarCliente : function(){

			var vData = $("#frmCliente").serialize();

			$.ajax({
				url:"/ventas/cliente/add",  
				type:'POST',
				async:true, 
				data: vData,
				beforeSend:function(){
					
				},
				success:function(respuesta){

					$("#mCliente").modal('hide');

					vVentas.getClientesDni();
          vVentas.getClientesRuc();
				}
			});
		},
		selectCliente : function(){

        var vNombre = "";
        var vDireccion = "";
        var vRuc = "";
        var vDni = "";

        if (vTipoDoc == "boleta") {

          vNombre = $("#dni_cliente option:selected").attr("data");
          vDireccion = $("#dni_cliente option:selected").attr("dir");
          vRuc =  $("#dni_cliente option:selected").attr("ruc");
          vDni = $("#dni_cliente option:selected").attr("dni");

        }else{

          vNombre = $("#ruc_cliente option:selected").attr("data");
          vDireccion = $("#ruc_cliente option:selected").attr("dir");
          vRuc =  $("#ruc_cliente option:selected").attr("ruc");
          vDni = $("#ruc_cliente option:selected").attr("dni");

        }

        $('#nom_cli').get(0).value = vNombre;
        $('#ruc_cli').get(0).value = vRuc;
        $('#dni_cli').get(0).value = vDni;
        $('#dir_cli').get(0).value = vDireccion;
    },
    getClientesDni : function(){

      $.ajax({
				url:"/ventas/ventas/get-clientes-dni",  
				type:'POST',
				async:true, 
				beforeSend:function(){
					
				},
				success:function(respuesta){

					$("#dni_cliente").html(respuesta);

				
				}
			});
    },
    getClientesRuc : function(){

      $.ajax({
        url:"/ventas/ventas/get-clientes-ruc",  
        type:'POST',
        async:true, 
        beforeSend:function(){
          
        },
        success:function(respuesta){

          $("#ruc_cliente").html(respuesta);

        
        }
      });
    },
    registrarVenta : function(){

          var vCliente = "";

          if (vTipoDoc == "boleta") {
            vCliente = vVentas.getValueSelect("#dni_cliente");
          }else{
            vCliente = vVentas.getValueSelect("#ruc_cliente");
          }

        	var vFechaReg = $("#fecha").val();
        	var vHoraReg = $("#hora").val();
        	var vCaja = $("#id_caja").val();
        	var vSubTotal = parseFloat($(".sub-total").html());
        	var vIgv = parseFloat($(".igv").html());
        	var vTotal = parseFloat($(".total").html());
        	var vFormPago = vVentas.getValueSelect("#f_pago");
        	var vTarjeta = $("#tarjeta option:selected").val();
        	var vDscto = parseFloat($(".dscto").html());
          var vTicket = $("#ticket").val();
          var vTipoCambio = $("#tipo_cambio").val();
          var vMoneda = $("#moneda option:selected").val();

        	var vProducto = vVentas.getProductosTabla();

        	var vVenta = {
        		id_cliente : vCliente,
        		fecha_reg : vFechaReg,
        		hora_reg : vHoraReg,
        		id_caja : vCaja,
        		subtotal : vSubTotal,
        		igv : vIgv,
        		total : vTotal,
        		f_pago : vFormPago,
        		tarjeta : vTarjeta,
        		dscto : vDscto,
            ticket : vTicket,
            tipo_doc : vTipoDoc,
            tipo_Cambio : vTipoCambio,
            moneda : vMoneda
        	};

        	$.ajax({
        			url:"/ventas/ventas/add",  
        			type:'POST',
        			async:true, 
        			data : {
        				venta : vVenta,
        				producto : vProducto
        			},
        			beforeSend:function(){
        				$("#process").modal('show');
        			},
        			success:function(respuesta){

        				$("#process").modal('hide');

                $("#imprime-ticket").html(respuesta);

                vVentas.imprimirTicket();

                $("#btn-n-venta").removeClass("disabled");

        			}
        	});
    },
    getValueSelect : function(vThis){

            var vIndex = $(vThis).get(0).selectedIndex;
            var vIdProv = $(vThis).get(0).options[vIndex].value;

            return vIdProv;
    },
    getProductosTabla : function(){
        	var vProdCompra = [];

            $("#tb_producto tbody tr").each(function (index) {

                var vIdProd = parseInt($(this).find("td:eq(2)").find("input").val()); 
                var vCant = parseInt($(this).find("td:eq(4)").find("input").val()); 
                var vPrec = $(this).find("td:eq(3)").find("input").val();
                var vSubTotal = $(this).find("td:eq(5)").html();

                var vData = {
                    id_prod : vIdProd,
                    precio : vPrec,
                    cantidad : vCant,
                    subtotal : vSubTotal
                };

                if (vCant != "0"  && $(this).find("td:eq(0)").html() != "No data available in table") {

                    vProdCompra.push(vData);
                }

            });

            /*if (vProdCompra.length === 0) {

                show_stack_custom_right("error","Agrega productos a la lista","Productos esta vacío || cantidad no puede ser 0");

            }*/

            return vProdCompra;
    },
    imprimirTicket : function(){

      $('#imprime-ticket').printArea();
    },
    deleteProducto : function(){

      $('#tb_producto tbody').on( 'click', '.btn-delete', function () {

            var vIdProd = $(this).parents('tr').find("td:eq(2) input").val();

            var vIndex = vProductos.indexOf(vIdProd);

            vProductos.splice(vIndex,1);

            vTableVentas
                .row( $(this).parents('tr') )
                .remove()
                .draw();

            vVentas.totalProductos();

            vVentas.calculaTotalPagar();
      });
    },
    tipoDocSelect : function(){

        $("input[name = 'tipo_doc']").click(function(){

            var vValue = $(this).attr("value");

            if (vValue == "factura") {


              $("#dni-tab").fadeOut(100,function(){

                $("#dni_cliente").prop('selectedIndex',0);

                
                $("#ruc-tab").fadeIn(100);

              });

            }else{

              $("#ruc-tab").fadeOut(100,function(){

                $("#ruc_cliente").get(0).selectedIndex = 0;

                $("#dni-tab").fadeIn(100);

              });

            }
        
            vTipoDoc = vValue;

            $('#nom_cli').get(0).value = "";
            $('#ruc_cli').get(0).value = "";
            $('#dni_cli').get(0).value = "";
            $('#dir_cli').get(0).value = "";
        });
    },
    validaRegistrar : function(){

      var vProdValida = vVentas.getProductosTabla();
      var vMonto = $("#m-pagar").val();
      var vMontoReci = $("#mont_ing").val();

      if ($("#moneda option:selected").val() == "dolares") {
        vMonto = vMonto.replace("$ ","");
        vMontoReci = vMontoReci.replace("$ ","");
      }else{
        vMonto = vMonto.replace("S/. ","");
        vMontoReci = vMontoReci.replace("S/. ","");

      }

      alert(vMonto +" "+ vMontoReci);

      if (vProdValida.length === 0) {

        swal({
          title: "No se pudo registrar la venta",
          text: "Productos esta vacío || cantidad no puede ser 0",
          type: "error",
          showCancelButton: false,
          showConfirmButton: false,
          timer: 3000,
        });

      }else{

      
       // $("#mPago").modal("hide");

        setTimeout(function(){

          //vVentas.registrarVenta();

        },1000);

      }
    },
    atajosTeclado : function(){

      $(document).jkey('f2', function() {
          vVentas.activarLectora();

          return false;
      });

      $(document).jkey('f3', function() {
          
          $("#mPago").modal("show");

          return false;
      });

      $(document).jkey('f4', function() {

          if ($("#btn-n-venta").hasClass("disabled") === false) {
              location.reload();
          }

          return false;
               
      });

      $(document).jkey('ctrl+b', function() {
        
        $("#mProducto").modal("show");

        $(".dataTables_filter input").focus();

        return false;
               
      });

      $(document).jkey('f5', function() {
        
        $("#mCaja").modal("show");

        return false;
               
      });

      $(document).jkey('f9', function() {
          
          vVentas.validaRegistrar();

          return false;
      });
    },
    cerrarCaja : function(){

      var vFechaReg = $("#fecha").val();
      var vHoraCierre = $("#hora").val();

      var vData = {
        hora_cierre : vHoraCierre,
        fecha : vFechaReg,
        id_caja : vCajaSession.id_caja,
        id_usuario : vCajaSession.id_usuario
      };

      $.ajax({
          url:"/ventas/caja/cerrar",  
          type:'POST',
          async:true, 
          data: {
            cierre : vData,
            caja : vCajaSession
          },
          beforeSend:function(){
            $("#mCaja .loader").fadeIn(100);
          },
          success:function(respuesta){

            swal({
                title: "Proceso realizado",
                text: "Se cerro caja",
                type: "success",
                showCancelButton: false,
                showConfirmButton: false,
                timer: 3000,
            });

            deleteSessionCaja();

            $("#mCaja .loader").fadeOut(100,function(){

              window.open("/ventas/ventas/reporte", '',"width=800, height=900");

              $("#mCaja").modal("hide");

              setTimeout(function(){
                 //location.href = "/";
              },5000);

            });

          }
        });
    },
    verAperturaCaja : function(){

      var vRol = $("#rol").val();

      if (vCajaSession.id_usuario !== undefined) {

        $("#nom_caja").html("CAJA : " + vCajaSession.nom_caja);
        $("#id_caja").val(vCajaSession.id_caja);

        vVentas.verTicket();

      }else{

        if (vRol == "cajero"){

          location.href = "/";

        }

      }
    },
    verTicket : function(){

      $.ajax({
        url:"/ventas/ventas/get-ticket",  
        type:'POST',
        async:true, 
        data : {
          caja: vCajaSession.id_caja,
        },
        beforeSend:function(){

        },
        success:function(respuesta){

          $("#ticket").val(respuesta);

        }
      });
    }

       
	};

	vVentas.init();
});