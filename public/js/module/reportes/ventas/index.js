$(function() {
	var vOpcion = "fecha";
	var vReporte = {
		init : function(){

			$(".styled").uniform({ radioClass: 'choice',wrapperClass: 'border-primary-600 text-primary-800'});

			vReporte.checkedReporte();

			$("#btn-ver-reporte").click(function(){

				vReporte.verReporte();

			});  

			$("#btn-rfecha").click(function(){

				vReporte.verReporteFecha();

			}); 

			vReporte.verReporteProv();
			vReporte.initControls();

			//configTable();
		},
		checkedReporte : function(){

			$("input[name = 'reporte']").click(function(){

				vOpcion = $(this).attr("value");

			});
		},
		verReporte : function(){

			switch(vOpcion) {

				case 'fecha':

					$("#mFecha").modal("show");

				break;

				case 'caja':

					$("#mCaja").modal("show");

				break;

				case 'prov':
					$("#mProveedor").modal("show");
				break;

			}
		},
		verReporteProv : function(){
			$('#tb_prov_modal tbody').on( 'click', '.aProveedor', function () {

				var vIdProv = $(this).parents('tr').find("td:eq(0)").html();

				window.open("/reportes/producto/proveedor/"+vIdProv, '',"width=800, height=900");

			});
		},
		verReporteFecha : function(){

			var vFechaIni = $("#fecha_ini_hidden").val();
			var vFechaFin = $("#fecha_fin_hidden").val();

			$("#mFecha").modal("hide");

			window.open("/reportes/ventas/fecha/null/"+vFechaIni+"/"+vFechaFin, '',"width=800, height=900");

		},
		verReporteCaja : function(){

			var vFechaIni = $("#fecha_ini_hidden").val();
			var vFechaFin = $("#fecha_fin_hidden").val();

			$("#mFecha").modal("hide");

			window.open("/reportes/ventas/fecha/"+vFechaIni+"/"+vFechaFin, '',"width=800, height=900");

		},
		initControls : function(){

			$('#fecha_ini').pickadate({
				labelMonthNext: 'Go to the next month',
				labelMonthPrev: 'Go to the previous month',
				labelMonthSelect: 'Pick a month from the dropdown',
				labelYearSelect: 'Pick a year from the dropdown',
				selectMonths: true,
				selectYears: 70,
				min: '',
				max: '',
				formatSubmit: 'yyyy-mm-dd'
			});

			$('#fecha_fin').pickadate({
				labelMonthNext: 'Go to the next month',
				labelMonthPrev: 'Go to the previous month',
				labelMonthSelect: 'Pick a month from the dropdown',
				labelYearSelect: 'Pick a year from the dropdown',
				selectMonths: true,
				selectYears: 70,
				min: '',
				max: new Date(),
				formatSubmit: 'yyyy-mm-dd'
			});
		}
	
	};

	vReporte.init();
});