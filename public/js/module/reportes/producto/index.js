$(function() {
	var vOpcion = "stock";
	var vReporte = {
		init : function(){

			$(".styled").uniform({ radioClass: 'choice',wrapperClass: 'border-primary-600 text-primary-800'});

			vReporte.checkedReporte();

			$("#btn-ver-reporte").click(function(){

				vReporte.verReporte();

			});  

			vReporte.verReporteProv();

			configTable();
		},
		checkedReporte : function(){

			$("input[name = 'reporte']").click(function(){

				vOpcion = $(this).attr("value");

			});
		},
		verReporte : function(){

			switch(vOpcion) {

				case 'stock':

					window.open("/reportes/producto/stock", '',"width=800, height=900");

				break;

				case 'sin-stock':
					window.open("/reportes/producto/stock", '',"width=800, height=900");
				break;

				case 'prov':
					$("#mProveedor").modal("show");
				break;

			}
		},
		verReporteProv : function(){
			$('#tb_prov_modal tbody').on( 'click', '.aProveedor', function () {

				var vIdProv = $(this).parents('tr').find("td:eq(0)").html();

				window.open("/reportes/producto/proveedor/"+vIdProv, '',"width=800, height=900");

			});
		}
	
	};

	vReporte.init();
});