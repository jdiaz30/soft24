$(function() {

	var vCajaSession = getSessionCaja();
	var options = {
		scaleColor: false,
		trackColor: 'rgba(255, 255, 255, 0.5)',
		barColor: '#fff',
		lineWidth: 8,
		lineCap: 'butt',
	};

	var vIndex = {
		init : function(){

			vIndex.modalAperturaCaja();
			
			alert(JSON.stringify(vCajaSession));
          
		},
		modalAperturaCaja : function(){

			if ($("#mCaja").length && vCajaSession.id_usuario === undefined) {

				$("#mCaja").modal("show");

				$(".monto").maskMoney();

				$(".btn-caja").click(function(){

					vIndex.aperturarCaja();

				});

				setInterval(function(){

					vIndex.verHora();

				}, 1000);

			}
		},
		verHora : function(){

			var crTime = new Date ( );
			var crHrs = crTime.getHours ( );
			var crMns = crTime.getMinutes ( );
			var crScs = crTime.getSeconds ( );
			crMns = ( crMns < 10 ? "0" : "" ) + crMns;
			crScs = ( crScs < 10 ? "0" : "" ) + crScs;
			var timeOfDay = ( crHrs < 12 ) ? "AM" : "PM";
			crHrs = ( crHrs > 12 ) ? crHrs - 12 : crHrs;
			crHrs = ( crHrs === 0 ) ? 12 : crHrs;
			var crTimeString = crHrs + ":" + crMns + ":" + crScs + " " + timeOfDay;

			$("#hora_aper").val(crTimeString);		
		},
		aperturarCaja : function(){

			var vData = $("#frmApertura").serialize();

			var vValidaMonto = vIndex.validaAperturaCaja();

			if (vValidaMonto != "0") {

				swal({
					title: "Proceso no realizado",
					text: "Ingrese los datos correctamente",
					type: "info",
					showCancelButton: false,
					showConfirmButton: false,
					timer: 3000,
				});

			}else{

				$.ajax({
					url:"/ventas/caja/add",  
					type:'POST',
					async:true, 
					data: vData,
					dataType :'json',
					beforeSend:function(){
						$(".loading").fadeIn(100);
					},
					success:function(respuesta){

						swal({
                            title: "Proceso realizado",
                            text: "Se aperturo caja",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 3000,
                        });

						localStorage.setItem("caja", JSON.stringify(respuesta));

						$(".loading").fadeOut(4000,function(){

							location.href = "/ventas/ventas/add";
						});

					}
				});

			}
		},
		validaAperturaCaja : function(){

			var vMonto = $("#monto_aper").val();
			var vMontoRep = $("#rep_monto").val();

			var vValida = 0;

			if (vMonto === "" || vMontoRep === ""){

				swal({
					title: "Ingresar monto",
					text: "Por favor ingrese el monto",
					type: "info",
					showCancelButton: false,
					showConfirmButton: false,
					timer: 3000,
				});

				vValida = 1;

			}else{

				if (vMontoRep != vMonto) {

					swal({
						title: "Monto no coinciden",
						text: "Por favor ingrese nuevamente el monto",
						type: "info",
						showCancelButton: false,
						showConfirmButton: false,
						timer: 3000,
					});

					vValida = 1;

				}

			}

		
			return vValida;
		}
	
	};

	vIndex.init();
});