$(function() {
	var vLogin = {
		init : function(){
			
            $( "#frmLogin" ).submit(function( event ) {

                event.preventDefault();

                if($("#user_name").get(0).value !=="" && $("#user_pass").get(0).value !==""){

                    vLogin.loguear();

                }
            });
          
		},
        loguear : function(){

            var vData = $("#frmLogin").serialize();

            $.ajax({
                url:"/login",  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal("show");
                },
                success:function(respuesta){

                    $("#process").modal("hide");

                    if (respuesta == "0") {

                        swal({
                            title: "Success!",
                            text: "Bienvenido al Sistema",
                            type: "success",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 3000,
                        });

                        setTimeout(function(){

                            location.reload();

                        },4000);
                        
                    }else{

                        swal({
                            title: "Error!",
                            text: "Credenciales no son correctas",
                            type: "warning",
                            showCancelButton: false,
                            showConfirmButton: false,
                            timer: 3000,
                        });
           
                    }
                 }
            });
        
	    }
    };

	vLogin.init();
});