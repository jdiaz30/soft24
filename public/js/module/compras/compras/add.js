$(function() {

    var vForm = null;
    var vTableProd = null;
    var vTbProdComp = null;
    var vProdList = [];
    var vIgvControl = null;

	var vCompra = {
		init : function(){

            vCompra.checkedOpcionProducto();

            vCompra.initControls();

           // vForm = init_validate_form("#frmCompras");

            $("#frmCompras").submit(function( event ) {

                event.preventDefault();

                if($(".btn-registrar").length){

                    var vProd = vCompra.capturaProductos();

                    var vValida = vCompra.validaRegistrar();

                    if(vProd.length === 0 || vValida >= 1){

                        swal({   title: "No se pudo registrar la compra",   text: "Necesitas llenar los datos de la compra correctamente!",   timer: 3000 ,type: "warning"});

                    }else{

                        vCompra.registrar("add",vProd);

                    }

                }else{

                    vCompra.registrar("edit");

                }

            });

            $("#id_prov").change(function(){

                vCompra.selectProveedor();

            });

            $('#igv').change(function(){
              vCompra.calculaIgvProductos();
            });

            $('#percepcion').keyup(function(){
              vCompra.agregarPercepcion();
            });

            vCompra.selectProveedor();
            vCompra.addProducto();
            vCompra.deleteProducto();
            vCompra.searchProducto();
           // vCompra.validaPrecioVenta();
            vCompra.calculaPrecioCompra();
            vCompra.calculaPrecioVenta();

		},
        registrar : function(proceso,vProd){

            var vData = $("#frmCompras").serialize();

            vData +="&productos="+JSON.stringify(vProd);

            $.ajax({
                url:"/compras/compras/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente la compra",   timer: 3000 ,type: "success"});

                    if (proceso == "add") {
                      
                       vCompra.clearForm();

                    }
           
                }
            });
        }, 
        selectProveedor : function(){

            var vRazonSocial = $("#id_prov option:selected").attr("data");

            $('#razon_social').get(0).value = vRazonSocial;

            var vIdProv = vCompra.getIdProveedor();

            vCompra.listarProdProv(vIdProv);
        },
        getIdProveedor : function(){

            var vIndex = $("#id_prov").get(0).selectedIndex;
            var vIdProv = $("#id_prov").get(0).options[vIndex].value;

            return vIdProv;
        },
        listarProdProv : function(vIdProv){

            $.ajax({
                url:"/compras/compras/get-product-by-prov",  
                type:'POST',
                async:true, 
                data: {
                    id_prov : vIdProv
                },
                beforeSend:function(){
                   
                },
                success:function(respuesta){

                    if (vTableProd !==null) {

                        vTableProd.destroy();
                    }

                    $("#tb_producto tbody").html(respuesta);

                    vTableProd = configTable("#tb_producto");  

                }
            });
        },
        checkedOpcionProducto : function(){

            $("#opLectora").click(function(){

                $("#id_prod").attr("readonly",false);
                $("#id_prod").focus();

            });

            $("#opManual").click(function(){

                $("#id_prod").attr("readonly",true);

            });
        },
        addProducto : function(){

            $('#tb_producto tbody').on( 'click', '.dropdown-menu.dropdown-menu-right li a', function () {

                var vIdProd = $(this).parents('tr').find("td:first").html();
                var vFoto = $(this).parents('tr').find("td:eq(1)").html();
                var vProd = vFoto +" </br>"+ $(this).parents('tr').find("td:eq(2)").html();
                var vFamilia = $(this).parents('tr').find("td:eq(4)").html();
                var vPrecio = $(this).parents('tr').find("td:eq(3)").html() || "0.00";

                var vCantidad = '<a href="#" class="cant" id="cant" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Cantidad">0</a>';
               // var vLote = '<a href="#" class="data-form" id="lote" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Lote">0</a>';
               // var vFecVenc = '<a href="#" class="date" id="date-venc" data-type="text" data-pk="1"  data-title="Fec. Venc">9999/99/99</a>';
                var vPrecComp = '<a href="#" class="data-form money" id="p-comp" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Precio compra">0.00</a>';
                var vValorUnit = '<a href="#" class="v-unit" id="v-unit" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Valor unitario">0.00</a>';
                var vDesc = '<a href="#" class="ganancia" id="desc" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Ganancia %">0.00</a>';
                var vIgv = '<a href="#" class="" id="igv" data-type="text" data-inputclass="form-control" data-pk="1" data-title="IGV">0.00</a>';
                var vPrecVen = '<a href="#" class="data-form money" id="p-venta" data-type="text" data-inputclass="form-control p-venta" data-pk="1" data-title="Precio venta">'+vPrecio+'</a>';
                var vIsc = '<a href="#" class="money" id="isc" data-type="text" data-inputclass="form-control" data-pk="1" data-title="ISC">0.00</a>';
               

                var vAction = '<ul class="icons-list"> '+
                '<li class="dropdown">'+
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>'+
                '<ul class="dropdown-menu dropdown-menu-right"><li><a href="#"><i class="icon-x"></i> Eliminar</a></li>'+
                '</ul></li></ul>';

                if (vProdList.indexOf(vIdProd) == -1) {

                    vProdList.push(vIdProd);

                    vTbProdComp.row.add( [vIdProd,vProd,vCantidad,vValorUnit,vDesc,vIsc,vIgv,vPrecComp,vPrecVen,vAction]).draw();

                    vCompra.initControlTableProd();
                  

                }
                 
              
            });
        },
        deleteProducto : function(){

            $('#tb_prod_comp tbody').on( 'click', '.dropdown-menu.dropdown-menu-right li a', function () {

                var vIdProd = $(this).parents('tr').find("td:first").html();

                var vIndex = vProdList.indexOf(vIdProd);

                vProdList.splice(vIndex,1);

                vTbProdComp
                .row( $(this).parents('tr') )
                .remove()
                .draw();

                vCompra.calculaTotalFactura();

            });
        },
        searchProducto : function(){

            $("#id_prod").keyup(function(){

                //console.log($(this).get(0).value);

                var vData = vTableProd.search( $(this).get(0).value).draw();

                console.log(vData);

            });
        },
        validaPrecioVenta : function(){
      
            $('#tb_prod_comp tbody').on( 'click', '.editable-buttons .editable-submit', function () {

                var vValida = $(this).parents('td').find("a").attr("data-title");

                if (vValida == "Precio venta") {

                    console.log(vValida);

                    var vPrecComp = parseFloat($(this).parents('tr').find("td:eq(8) a").html().replace(",",""));
                    var vPrecVent = parseFloat($(this).parents('tr').find("td:eq(9) input").val().replace(",",""));

                    console.log(vPrecComp +" / "+ vPrecVent );

                    if (vPrecComp >= vPrecVent) {

                        swal({   title: "Alerta",   text: "Precio compra tiene que ser menor que precio de venta",   timer: 3000 ,type: "warning"});

                    }
                }

            });
        },
        capturaProductos : function(){

            var vProdCompra = [];

            $("#tb_prod_comp tbody tr").each(function (index) {

                var vIdProd = $(this).find("td:eq(0)").html();
               // var vFecVenc = $(this).find("td:eq(2) a").html();
                //var vLote = $(this).find("td:eq(2) a").html();
                var vCant = $(this).find("td:eq(2) a").html();
                var vPrecComp = $(this).find("td:eq(7) a").html();
                var vValorUnit = $(this).find("td:eq(3) a").html();
                var vDesc = $(this).find("td:eq(4) a").html();
                var vIsc = $(this).find("td:eq(5) a").html();
                var vIgv = $(this).find("td:eq(6) a").html();
                var vPrecVent = $(this).find("td:eq(8) a").html();

                var vData = {

                    id_prod : vIdProd,
                    //lote : vLote,
                    cantidad : vCant,
                    val_unit : vValorUnit,
                    igv : vIgv,
                    prec_comp : vPrecComp,
                    prec_vent : vPrecVent,
                    descuento : vDesc,
                    isc : vIsc,
                    fecha_venc : ""

                };

                if (vIdProd != "No data available in table" && vCant != "0" && vPrecComp != "0.00" && vPrecVent != "0.00" && vValorUnit != "0.00") {

                    vProdCompra.push(vData);
                }

            });

            if ($("#tb_prod_comp tbody tr").length >= 1 && vProdCompra.length === 0) {

                swal({   title: "No hay productos || Datos de productos",   text: " Datos no estan correctos",   timer: 3000 ,type: "warning"});
            }

            console.log(JSON.stringify(vProdCompra));

            return vProdCompra;
        },
        initControlTableProd : function(){

            //console.log($(".cant").length);

            //if ($(".cant").length <= 1) {

                $('.data-form').editable({
                    validate: function(value) {
                        if($.trim(value) === '') return 'Este campo es requerido';
                    }
                });

                /*$('.date').editable({
                    showbuttons: 'bottom',
                    validate: function(value) {
                        if($.trim(value) === '') return 'Este campo es requerido';
                    }

                });

                $('.date').on('shown', function(e, editable) {
                    editable.input.$input.formatter({
                    pattern: '{{9999}}/{{99}}/{{99}}'
                    });
                    
                });*/

                $('.cant').editable({
                    clear: false,
                    validate: function(value) {
                        if($.trim(value) === '' || $.trim(value) === '0') return 'Este campo es requerido';
                    }
                });

                $('.cant').on('shown', function(e, editable) {
                    editable.input.$input.TouchSpin({
                        min: 0,
                        max: 9000,
                        step: 1,
                        decimals: 0
                    }).parent().parent().addClass('editable-touchspin');
                });


                $('.ganancia').editable({
                    clear: false,
                    validate: function(value) {
                        if($.trim(value) === '' || $.trim(value) === '0') return 'Este campo es requerido';
                    }
                });

                $('.ganancia').on('shown', function(e, editable) {
                    editable.input.$input.TouchSpin({
                        min: 0,
                        max: 100,
                        step: 1,
                        decimals: 0
                    }).parent().parent().addClass('editable-touchspin');
                });


        
                $('.money').editable();

                $('.money').on('shown', function(e, editable) {
                    editable.input.$input.maskMoney();

                });

                $('.v-unit').editable();

                $('.v-unit').on('shown', function(e, editable) {
                    editable.input.$input.maskMoney({precision : 4});

                });

           // }
        },
        validaRegistrar : function(){

            var vValidaReg = 0;
            var vIdProv = vCompra.getIdProveedor();

            var vFact = $("#n_fact").get(0).value;
            var vGuia = $("#n_guia").get(0).value;

            var vFecEmi = $("#fecha_emi").get(0).value;
            var vFecRec = $("#fecha_rec").get(0).value;

            if (vFecEmi === "") {

                $("#fecha_emi").parents('.form-group').addClass("has-error");
                vValidaReg = 1;

            }else{

                $("#fecha_emi").parents('.form-group').removeClass("has-error");

            }

            if (vFecRec === "") {

                $("#fecha_rec").parents('.form-group').addClass("has-error");
                 vValidaReg = 1;

            }else{

                $("#fecha_rec").parents('.form-group').removeClass("has-error");

            }

            if (vFact === "") {

                $("#n_fact").parents('.form-group').addClass("has-error");
                vValidaReg = 1;

            }else{

                var vValidaFact = vCompra.validaNFactura(vIdProv,vFact);

                if (vValidaFact !== "0") {

                    $("#n_fact").parents('.form-group').addClass("has-error");

                    show_stack_custom_right("error","Alerta","El N° de factura ya existe");

                    vValidaReg = 1;

                }else{

                    $("#n_fact").parents('.form-group').removeClass("has-error");

                }

            }

            if (vGuia !== ""){

                var vValidaGuia = vCompra.validaNGuia(vIdProv,vGuia);

                if (vValidaGuia !== "0") {

                    $("#n_guia").parents('.form-group').addClass("has-error");
                    
                    swal({   title: "Alerta",   text: "El N° de guia ya existe",   timer: 3000 ,type: "warning"});

                    vValidaReg = 1;

                }else{

                     $("#n_guia").parents('.form-group').removeClass("has-error");

                } 

            }


            return vValidaReg;
        },
        validaNFactura : function(vIdProv,vNFact){

            var vRes = "";

            $.ajax({
                url:"/compras/compras/valida-factura",  
                type:'POST',
                async:false, 
                data: {
                    id_prov : vIdProv,
                    n_fact : vNFact
                },
                beforeSend:function(){
                    //$("#process").modal('show');
                },
                success:function(respuesta){

                   vRes = respuesta;
                }
            });

            return vRes;
        },
        validaNGuia : function(vIdProv,vGuia){

            var vRes = "";

            $.ajax({
                url:"/compras/compras/valida-guia",  
                type:'POST',
                async:false, 
                data: {
                    id_prov : vIdProv,
                    n_guia : vGuia
                },
                beforeSend:function(){
                    //$("#process").modal('show');
                },
                success:function(respuesta){

                   vRes = respuesta;
                }
            });

            return vRes;
        },
        clearForm : function(){

            $("#frmCompras").trigger('reset');

            $("#total-text").html("S/. 0.00");

            $("#total").val("");

            vTbProdComp.clear().draw();

            vCompra.selectProveedor();

            vProdList.length = 0;
        },
        initControls : function(){

           $('.pickadate-accessibility').daterangepicker({ 
            singleDatePicker: true, 
            locale: {
                format: 'DD-MM-YYYY'
            }}, function(start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            vTbProdComp = configTable("#tb_prod_comp");

            $('.ndoc').formatter({
                pattern: '{{999}} - {{9999999}}'
            });

            $("#percepcion").maskMoney();
        },
        calculaPrecioCompra : function(vThis){

            $('#tb_prod_comp tbody').on( 'click', '.editable-buttons .editable-submit', function () {

                var vValida = $(this).parents('td').find("a").attr("data-title");
                var vPrecUnit = 0;
                var vCantidad = 0;

                if (vValida == "Valor unitario" || vValida == "Cantidad") {

                    if (vValida == "Cantidad") {

                        console.log("cantidad");

                        vCantidad = parseInt($(this).parents('tr').find("td:eq(2) input").val());
                        vPrecUnit = parseFloat($(this).parents('tr').find("td:eq(3) a").html().replace(",",""));
                         console.log(vCantidad +" " + vPrecUnit);

                    }else{

                        vCantidad = parseInt($(this).parents('tr').find("td:eq(2) a").html());
                        vPrecUnit = parseFloat($(this).parents('tr').find("td:eq(3) input").val().replace(",",""));

                    }
                    

                    var vPrecComp = vCantidad * vPrecUnit;

                    var vIgvCalculado = 0;

                    var vIgv = 0.18;

                    if( $('#igv').is(':checked') ) {

                        vIgvCalculado = (vPrecComp / 1.18) * vIgv ;
                        vIgvCalculado = vIgvCalculado.toFixed(2);

                        $(this).parents('tr').find("td:eq(6) a").html(vIgvCalculado);
                       
                    }else{
                        vIgvCalculado = vPrecComp *  vIgv ;

                        vIgvCalculado = vIgvCalculado.toFixed(2);

                        $(this).parents('tr').find("td:eq(6) a").html(vIgvCalculado);

                        vPrecComp = parseFloat(vPrecComp) + parseFloat(vIgvCalculado);
    
                    }

                    $(this).parents('tr').find("td:eq(7) a").html(vPrecComp.toFixed(2));

                    vCompra.calculaTotalFactura();
                }

            });
        },
        calculaPrecioVenta : function(vThis){

            $('#tb_prod_comp tbody').on( 'click', '.editable-buttons .editable-submit', function () {

                var vValida = $(this).parents('td').find("a").attr("data-title");

                if (vValida == "Ganancia %") {

                    var vPrecUnit = parseFloat($(this).parents('tr').find("td:eq(3) a").html().replace(",",""));

                    if (vPrecUnit == "0.00") {

                        show_stack_custom_right("error","Alerta","Ingrese precio unitario");

                    }else{

                        var vGanancia = parseFloat($(this).parents('tr').find("td:eq(4) input").val());

                        var vPrecVent = (vPrecUnit * (vGanancia / 100) ) + vPrecUnit;

                        vPrecVent = vPrecVent.toFixed(2);

                        vPrecVent = decimalesTienda(vPrecVent);

                        $(this).parents('tr').find("td:eq(8) a").html(vPrecVent);

                    }


                }

            });
        },
        calculaIgvProductos : function(){

            $("#tb_prod_comp tbody tr").each(function (index) {

                var vCantidad = parseInt($(this).find("td:eq(2) a").html());
                var vPrecUnit = parseFloat($(this).find("td:eq(3) a").html().replace(",",""));
                var vIgvCalculado = 0;

                var vPrecComp = vCantidad * vPrecUnit ;

                var vIgv = 0.18;

                $(this).find("td:eq(7) a").html(vPrecComp);

                if( $('#igv').prop('checked')) {

                    vIgvCalculado = (vPrecComp / 1.18) * vIgv ;
                    vIgvCalculado = vIgvCalculado.toFixed(2);


                    $(this).find("td:eq(6) a").html(vIgvCalculado);

                }else{
                    vIgvCalculado = vPrecComp *  vIgv ;
                    vIgvCalculado = vIgvCalculado.toFixed(2);

                    $(this).find("td:eq(6) a").html(vIgvCalculado);

                    vPrecComp = parseFloat(vPrecComp) + parseFloat(vIgvCalculado);

                    
                }

                 $(this).find("td:eq(7) a").html(vPrecComp.toFixed(2));

            });

            vCompra.calculaTotalFactura();
        },
        calculaTotalFactura : function(){

            var vTotal = 0.00;
            var vPercepcion = $("#percepcion").get(0).value;

            $("#tb_prod_comp tbody tr").each(function (index) {

                if ($(this).find("td:eq(0)").html()!= "No data available in table") {

                    var vPrecComp = parseFloat($(this).find("td:eq(7) a").html().replace(",",""));

                    vTotal  += vPrecComp ;

                }

            });

            if((vPercepcion !=="0.00" && vPercepcion !=="0" && vPercepcion !== "" ) && vTotal !== 0){

                vTotal = vTotal + parseFloat(vPercepcion);

            }

            //if (vTotal !== 0) {
            vTotal =  vTotal.toFixed(2);
               
            //}

            $("#total-text").html("S/. " + vTotal);
            $("#total").val(vTotal);
        },
        agregarPercepcion : function(){

            if ($("#percepcion").get(0).value !== "0.00" || $("#percepcion").get(0).value !== "0" ) {
                 
                vCompra.calculaTotalFactura();
  
            }

        }

    
	};

	vCompra.init();
});