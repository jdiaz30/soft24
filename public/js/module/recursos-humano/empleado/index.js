$(function() {

	var vEmpleado = {
		init : function(){

            $(".btn-search").click(function(){

                var vNombre = $("#search").get(0).value;
                vEmpleado.search(vNombre);

            });
		
		},
        search : function(vNombre){

            $.ajax({
                url:"/recursos-humano/empleado/search-by-name",  
                type:'POST',
                async:true, 
                data: {
                    nombre : vNombre
                },
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    $("#search-content").html(respuesta);
           
                }
            });
        }
         
	};

	vEmpleado.init();

});