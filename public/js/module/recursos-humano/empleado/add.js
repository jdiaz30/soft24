$(function() {

    var vForm = null;
	var vEmpleado = {
		init : function(){
		
           /* $('.select').select2({
                minimumResultsForSearch: "-1"
            });*/

            $('.pickadate-accessibility').daterangepicker({ 
                singleDatePicker: true, 
                locale: {
                    format: 'DD-MM-YYYY'
                }}, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });

            vForm = init_validate_form("#frmEmpleado");

            $("#frmEmpleado").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length >= 4) {

                    if($(".btn-registrar").length){

                        vEmpleado.registrar("add");

                    }else{

                        vEmpleado.registrar("edit");

                    }

                }

            });

            vEmpleado.fotoInit();
		},
        registrar : function(proceso){

            var vData = $("#frmEmpleado").serialize();

            $.ajax({
                url:"/recursos-humano/empleado/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});
                     
                    vForm.resetForm();

                    if (proceso == "add") {

                        $("#foto").get(0).value = "";
                        $("#img-prod").attr("src","http://"+document.location.hostname+"/img/user.png");

                        $("#frmEmpleado").trigger('reset');
                       
                    }
           
                }
            });
        }, 
        fotoInit : function(){
            var button = $(".btn-foto");
   
            new AjaxUpload(button, {
                action: "/recursos-humano/empleado/image-upload",
                onSubmit : function(file , ext){
                if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                    // extensiones permitidas
                    swal({   title: "Alerta",   text: "Solo se permiten imagenes",   timer: 3000 ,type: "info"});
                     
                    // cancela upload
                    return false;
                } else {
                      
                    this.disable();

                }
                }, 
                onComplete: function(file, response){
                    
                    // habilito upload button                       
                    this.enable();          
                    // Agrega archivo a la lista

                    $("#foto").get(0).value = response;

                    $("#img-prod").attr("src","http://"+document.location.hostname+"/img/usuarios/"+response);
                   
           
                }   
            });
        }
      
	};

	vEmpleado.init();
});