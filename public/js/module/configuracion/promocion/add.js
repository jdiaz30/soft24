$(function() {

    var vForm = null;
    var vTableProdPro = null;
    var vTableProdModal = null;
    var vProductos = [];
    var vCombo = "no";
	var vPromocion = {
		init : function(){

            vPromocion.initControls();

            vTableProdPro = configTable("#tb_prod_promo");

            vTableProdModal = configTable("#tb_prod_modal");

            if($(".btn-editar").length){

                vPromocion.initControlTableProd();

                var vListaProd = $("#id_prod_list").get(0).value;

                vProductos = vListaProd.split(",");

                vCombo = $("#combo_pro").get(0).value;

            }

            $("#frmPromo").submit(function( event ) {

                event.preventDefault();

                var vProd = vPromocion.capturaProductos();


                if(vProd.length === 0){

                    swal({   title: "No se pudo registrar la promoción",   text: "Necesitas llenar los datos de la promoción correctamente!",   timer: 3000 ,type: "warning"});

                }else{

                    if (vCombo == "si" && vProd.length < 2) {

                        swal({   title: "No se pudo registrar la promoción",   text: "Necesitas agregar 2 productos para el combo!",   timer: 3000 ,type: "warning"});

                    }else{

                        if($(".btn-registrar").length){

                            vPromocion.registrar("add",vProd);

                        }else{

                            vPromocion.registrar("edit",vProd);

                        }

                        
                    }


                }

            });

            vPromocion.addProducto();
            vPromocion.calculaTotal();
            vPromocion.comboCheck();
            vPromocion.deleteProducto();

		},
        initControls : function(){

          $('.pickadate-accessibility').daterangepicker({ 
            singleDatePicker: true, 
            locale: {
                format: 'DD-MM-YYYY'
            }}, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            });

            //$(".styled").uniform({ radioClass: 'choice' });

            /*$('.select').select2({
                minimumResultsForSearch: "-1"
            });*/
        },
        addProducto : function(){

            $('#tb_prod_modal tbody').on( 'click', '.dropdown-menu.dropdown-menu-right li a', function () {

                var vIdProd = $(this).parents('tr').find("td:first").html();
                var vFoto = $(this).parents('tr').find("td:eq(2)").html();
                var vProd = vFoto +" </br>"+ $(this).parents('tr').find("td:eq(3)").html();
     
                var vCantidad = '<a href="#" class="cant" id="cant" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Cantidad">0</a>';
                var vPrecVen = $(this).parents('tr').find("td:eq(4)").html();
                var vPrecOferta = '<a href="#" class="data-form money" id="p-venta" data-type="text" data-inputclass="form-control p-venta" data-pk="1" data-title="Precio venta">0.00</a>';
           
                var vAction = '<ul class="icons-list"> '+
                '<li class="dropdown">'+
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>'+
                '<ul class="dropdown-menu dropdown-menu-right"><li><a href="#"><i class="icon-x"></i> Eliminar</a></li>'+
                '</ul></li></ul>';

                var vTotal = "0.00";

                if (vProductos.indexOf(vIdProd) == -1) {

                    if (vCombo == "no" && vProductos.length >=1) {

                        swal({   title: "Info",   text: "Selecciona combo para agregar mas productos",   timer: 3000 ,type: "info"});
                     
                    }else{

                     vTableProdPro.row.add( [vIdProd,vProd,vCantidad,vPrecVen,vTotal,vPrecOferta,vAction]).draw();

                     vProductos.push(vIdProd);

                     vPromocion.initControlTableProd();

                    }

                }


            });
        },
        initControlTableProd : function(){

                $('.data-form').editable({
                    validate: function(value) {
                        if($.trim(value) === '') return 'Este campo es requerido';
                    }
                });

         
                $('.cant').editable({
                    clear: false,
                    validate: function(value) {
                        if($.trim(value) === '' || $.trim(value) === '0') return 'Este campo es requerido';
                    }
                });

                $('.cant').on('shown', function(e, editable) {
                    editable.input.$input.TouchSpin({
                        min: 0,
                        max: 100,
                        step: 1,
                        decimals: 0
                    }).parent().parent().addClass('editable-touchspin');
                });

                $('.money').editable();

                $('.money').on('shown', function(e, editable) {
                    editable.input.$input.maskMoney();

                });
        },
        calculaTotal : function(){
            $('#tb_prod_promo tbody').on( 'click', '.editable-buttons .editable-submit', function () {

                var vValida = $(this).parents('td').find("a").attr("data-title");

                if (vValida == "Cantidad") {
                    
                    var vCantidad = parseInt($(this).parents('tr').find("td:eq(2) input").val());

                    var vPrecUnit = parseFloat($(this).parents('tr').find("td:eq(3)").html().replace(",",""));

                    var vTotal = vCantidad * vPrecUnit;

                    $(this).parents('tr').find("td:eq(4)").html(vTotal.toFixed(2));


                }
            });
        },
        comboCheck : function(){

            $("input[name = 'combo']").click(function(){

                var vValue = $(this).attr("value");

                vPromocion.calculaTotalCombo();
        
                vCombo = vValue;
            });
        },
        calculaTotalCombo : function(){

            var vTotalCombo = 0;

            $("#tb_prod_promo tbody tr").each(function (index) {

                if ($(this).find("td:eq(0)").html()!= "No data available in table") {

                    vTotalCombo+= parseFloat($(this).find("td:eq(5) a").html().replace(",","")); 

                }

            });

            vTotalCombo = vTotalCombo.toFixed(2);

            $("#total_combo").val(vTotalCombo);
        },
        deleteProducto : function(){

            $('#tb_prod_promo tbody').on( 'click', '.dropdown-menu.dropdown-menu-right li a', function () {

                var vIdProd = $(this).parents('tr').find("td:first").html();

                var vIndex = vProductos.indexOf(vIdProd);

                vProductos.splice(vIndex,1);

                vTableProdPro
                .row( $(this).parents('tr') )
                .remove()
                .draw();

                vPromocion.calculaTotalCombo();

            });
        },
        registrar : function(proceso,vProd){

            var vData = $("#frmPromo").serialize();

             vData +="&productos="+JSON.stringify(vProd);

            $.ajax({
                url:"/configuracion/promocion/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente la promoción",   timer: 3000 ,type: "success"});

                    setTimeout(function(){

                        //vPromocion.clearForm();

                    },3000);
           
                }
            });
        },
        capturaProductos : function(){

            var vProdPromo= [];

            $("#tb_prod_promo tbody tr").each(function (index) {

                var vIdProd = $(this).find("td:eq(0)").html();
                var vCant = $(this).find("td:eq(2) a").html();
                var vPrecUnit= $(this).find("td:eq(3)").html();
 
                var vPrecOferta = $(this).find("td:eq(5) a").html();

                var vData = {

                    id_prod : vIdProd,
                    cantidad : vCant,
                    precio : vPrecUnit,
                    precio_oferta : vPrecOferta,
            

                };

                if (vIdProd != "No data available in table" && vCant != "0" && vPrecUnit != "0.00" && vPrecOferta != "0.00") {

                    vProdPromo.push(vData);
                }

            });

            if ($("#tb_prod_promo tbody tr").length >= 1 && vProdPromo.length === 0) {

                swal({   title: "Datos no estan correctos",   text: "No hay productos || Datos de productos",   timer: 3000 ,type: "warning"});

            }


            return vProdPromo;
        },
        clearForm : function(){

            /*vTableProdPro.clear().draw();
            vProductos.length = 0;
            vCombo = "no";*/

            location.reload();

        }

     
      
	};

	vPromocion.init();
});