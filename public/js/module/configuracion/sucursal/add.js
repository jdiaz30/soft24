$(function() {

    var vForm = null;
	var vSucursal = {
		init : function(){
			
           /* $('.select').select2({
                minimumResultsForSearch: "-1"
            });*/

            vForm = init_validate_form("#frmSucursal");

            $("#frmSucursal").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length >= 3) {

                    if($(".btn-registrar").length){

                        vSucursal.registrar("add");

                    }else{

                        vSucursal.registrar("edit");

                    }

                }

            });
          
		},
        registrar : function(proceso){

           var vData = $("#frmSucursal").serialize();

            $.ajax({
                url:"/configuracion/sucursal/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Success!",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    if (proceso == "add") {
                        $("#frmSucursal").trigger('reset');
                        vForm.resetForm();
                    }
           
                }
            });
        },   
		
	};

	vSucursal.init();
});