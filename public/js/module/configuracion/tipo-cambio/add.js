$(function() {
	var vTipoCambio = {
		init : function(){

			$("#valor").maskMoney();

			$(".btn-registrar").click(function(){

				vTipoCambio.registrar("add");

			});

			$(".btn-editar").click(function(){

				vTipoCambio.registrar("edit");

			});
          
		},
		registrar : function(proceso){

			var vValor = $("#valor").val();

			var vData = $("#frmTipoCambio").serialize();

			if (vValor !="0.00" && vValor !=="") {

				$.ajax({
					url:"/configuracion/tipo-cambio/"+proceso,  
					type:'POST',
					async:true, 
					data : vData,
					beforeSend:function(){
						$("#process").modal('show');
					},
					success:function(respuesta){

						$("#process").modal('hide');

						swal({   title: "Success!",   text: "Se registro el tipo de cambio",   timer: 3000 ,type: "success"});


						setTimeout(function(){

							location.href = "/configuracion/tipo-cambio";

						},2000);


					}
				});

			}else{

				swal({   title: "Error!",   text: "Tipo de cambio invalido",   timer: 3000 ,type: "error"});


			}
		}
   
	};

	vTipoCambio.init();
});