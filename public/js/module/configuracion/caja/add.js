$(function() {

    var vForm = null;
	var vCaja = {
		init : function(){
		    
            vCaja.initControls();

            vForm = init_validate_form("#frmCaja");

            $("#frmCaja").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length == 1) {

                    if($(".btn-registrar").length){

                        vCaja.registrar("add");

                    }else{

                        vCaja.registrar("edit");

                    }

                }

            });
          
		},
        registrar : function(proceso){

           var vData = $("#frmCaja").serialize();

            $.ajax({
                url:"/configuracion/caja/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Success!",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    vForm.resetForm();

                    if (proceso == "add") {
                        $("#frmCaja").trigger('reset');
                       
                    }
           
                }
            });
        },
        initControls : function(){

            $('#nom_caja').formatter({
                pattern: '{{999}}'
            });


        } 
		
	};

	vCaja.init();
});