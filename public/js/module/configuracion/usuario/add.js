$(function() {

    var vForm = null;
    var vModulo = [];
	var vUsuario = {
		init : function(){
         
            /*$('.select').select2({
                minimumResultsForSearch: "-1"
            });*/

            //$('.select-search').select2();

            vForm = init_validate_form("#frmUsuario");

            $("#frmUsuario").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length >= 2) {

                    if($(".btn-registrar").length){

                        vUsuario.registrar("add");

                    }else{

                        vUsuario.registrar("edit");

                    }

                }

            });

          
		},
        registrar : function(proceso){

           var vData = $("#frmUsuario").serialize();

            $.ajax({
                url:"/configuracion/usuario/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Success!",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    vForm.resetForm();

                    if (proceso == "add") {

                        $("#frmUsuario").trigger('reset');

                        $.uniform.restore(".control-success");
   
                    }
           
                }
            });
        }, 

		
	};

	vUsuario.init();
});