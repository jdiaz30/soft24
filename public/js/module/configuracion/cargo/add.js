$(function() {

    var vForm = null;
	var vCargo = {
		init : function(){
			
            /*$('.select').select2({
                minimumResultsForSearch: "-1"
            });*/

            vForm = init_validate_form("#frmCargo");

            $("#frmCargo").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length == 1) {

                    if($(".btn-registrar").length){

                        vCargo.registrar("add");

                    }else{

                        vCargo.registrar("edit");

                    }

                }

            });
          
		},
        registrar : function(proceso){

           var vData = $("#frmCargo").serialize();

            $.ajax({
                url:"/configuracion/cargo/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Success!",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    if (proceso == "add") {
                        $("#frmCargo").trigger('reset');
                        vForm.resetForm();
                    }
           
                }
            });
        },   
		
	};

	vCargo.init();
});