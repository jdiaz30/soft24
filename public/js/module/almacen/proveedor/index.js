$(function() {
	var vProveedor = {
		init : function(){
			
          	configTable();

          	vProveedor.activeId();

          	$("#btn-si").click(function(){

				vProveedor.deleteProd();

			});
          
		},

		activeId : function(){

			$(".delete-prov").click(function(){

				var vId = $(this).attr("data");

				$("#id_prov").get(0).value = vId;

			});

		},
		deleteProd : function(){

			var vId = $("#id_prov").get(0).value;

			$.ajax({
                url:"/almacen/proveedor/delete",  
                type:'POST',
                async:true, 
                data: {
                	id_prov : vId
                },
                beforeSend:function(){
                    $(".modal-body p").fadeOut(150,function(){

                    	$(".loading").fadeIn(100);

                    });
                },
                success:function(respuesta){

                    $("#mDelete").modal('hide');

                    $(".loading").fadeOut(150,function(){

                    	$(".modal-body p").fadeIn(100);

                    });

                    if (respuesta !="error") {

                        swal({   title: "Proceso realizado",   text: "Se elimino el registro correctamente",   timer: 3000 ,type: "success"});

                    	location.reload();

                    }else{

                        swal({   title: "Proceso no realizado",   text: "No se pudo eliminar el registro",   timer: 3000 ,type: "success"});

                    }

                    
           
                }
            });

		}
   
		
	};

	vProveedor.init();
});