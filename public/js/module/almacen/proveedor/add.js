$(function() {

    var vForm = null;
	var vProveedor = {
		init : function(){
			
            vForm = init_validate_form("#frmProveedor");

            $("#frmProveedor").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length == 6) {

                    if($(".btn-registrar").length){

                        vProveedor.registrar("add");

                    }else{

                        vProveedor.registrar("edit");

                    }

                }

            });

            vProveedor.fotoInit();
          
		},
        registrar : function(proceso){

           var vData = $("#frmProveedor").serialize();

            $.ajax({
                url:"/almacen/proveedor/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    if (proceso == "add") {
                        $("#frmProveedor").trigger('reset');
                        vForm.resetForm();
                    }
           
                }
            });
        },
        fotoInit : function(){
            var button = $(".btn-foto");
   
            new AjaxUpload(button, {
                action: "/almacen/proveedor/image-upload",
                onSubmit : function(file , ext){
                if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                    // extensiones permitidas
                    alert('Error: Solo se permiten imagenes');
                    // cancela upload
                    return false;
                } else {
                      
                    this.disable();

                }
                }, 
                onComplete: function(file, response){
                    
                    // habilito upload button                       
                    this.enable();          
                    // Agrega archivo a la lista

                    $("#logo").get(0).value = response;

                    $("#img-prod").attr("src","http://"+document.location.hostname+"/img/proveedores/"+response);
                   
           
                }   
            });
        },
    
		
	};

	vProveedor.init();
});