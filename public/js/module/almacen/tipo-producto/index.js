$(function() {
	var vTipoProd = {
		init : function(){
			configTable();

			vTipoProd.activeId();

			$("#btn-si").click(function(){

				vTipoProd.deleteTipo();

			});
		},
		activeId : function(){

			$(".delete-tipo").click(function(){

				var vId = $(this).attr("data");

				$("#id_tipo_prod").get(0).value = vId;


			});

		},
		deleteTipo : function(){

			var vId = $("#id_tipo_prod").get(0).value;

			$.ajax({
                url:"/almacen/tipo-producto/delete",  
                type:'POST',
                async:true, 
                data: {
                	id_tipo_prod : vId
                },
                beforeSend:function(){
                    $(".modal-body .text").fadeOut(150,function(){

                    	$(".modal-body .loader").fadeIn(100);

                    });
                },
                success:function(respuesta){

                    $("#mDelete").modal('hide');

                    $(".modal-body .loader").fadeOut(150,function(){

                    	$(".modal-body .text").fadeIn(100);

                    });

                    if (respuesta !="error") {

                        swal({   title: "Proceso realizado",   text: "Se elimino el registro correctamente",   timer: 3000 ,type: "success"});

                    	location.reload();

                    }else{

                        swal({   title: "Proceso no realizado",   text: "No se pudo eliminar el registro",   timer: 3000 ,type: "warning"});

                    }

                }
            });

		}
   
	
	};

	vTipoProd.init();
});