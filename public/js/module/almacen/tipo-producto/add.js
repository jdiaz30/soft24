$(function() {
	var vTipoProd = {
		init : function(){
			
            $(".btn-registrar").click(function(){
              vTipoProd.registrar("add");
            });

            $(".btn-editar").click(function(){
              vTipoProd.registrar("edit");
            });
   
		},
        registrar : function(proceso){

           var vData = $("#frmTipo").serialize();

            $.ajax({
                url:"/almacen/tipo-producto/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                }
            });
        }
	
	};

	vTipoProd.init();
});