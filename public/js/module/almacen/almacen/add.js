$(function() {

    var vForm = null;
	var vAlmacen = {
		init : function(){
			
            vForm = init_validate_form("#frmAlmacen");

            $("#frmAlmacen").submit(function( event ) {

                event.preventDefault();

                if ($(".validation-error-label.validation-valid-label").length == 1) {

                    var vSucursal = $("#sucursal").get(0).value;

                    if ( vSucursal !=="") {

                       if($(".btn-registrar").length){

                            vAlmacen.registrar("add");

                        }else{

                            vAlmacen.registrar("edit");

                        }

                    }

                }

            });

            configTable();

            $(".aSucursal").click(function(e){

                vAlmacen.addSucursal(this);

            });
          
		},
        registrar : function(proceso){

           var vData = $("#frmAlmacen").serialize();

            $.ajax({
                url:"/almacen/almacen/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    if (proceso == "add") {
                        $("#frmAlmacen").trigger('reset');
                        vForm.resetForm();
                    }
           
                }
            });
        },   
        addSucursal : function(vThis){

            $("#sucursal").get(0).value = $(vThis).attr("data-desc");
            $("#id_sucursal").get(0).value = $(vThis).attr("data-id");

            $("#mSucursal").modal("hide");

        },
		
	};

	vAlmacen.init();
});