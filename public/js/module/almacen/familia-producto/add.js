$(function() {
	var vFamilia = {
		init : function(){
			
            $(".btn-registrar").click(function(){
              vFamilia.registrar("add");
            });

            $(".btn-editar").click(function(){
              vFamilia.registrar("edit");
            });
          
		},
        registrar : function(proceso){

           var vData = $("#frmFamilia").serialize();

            $.ajax({
                url:"/almacen/familia-producto/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                }
            });
        }
	
	};

	vFamilia.init();
});