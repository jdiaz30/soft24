$(function() {
	var vFamilia = {
		init : function(){

			configTable();

			vFamilia.activeId();

			$("#btn-si").click(function(){

				vFamilia.deleteFamilia();

			});
          
		},
		activeId : function(){

			$(".delete-fam").click(function(){

				var vId = $(this).attr("data");

				$("#id_familia").get(0).value = vId;

			});

		},
		deleteFamilia : function(){

			var vId = $("#id_familia").get(0).value;

			$.ajax({
                url:"/almacen/familia-producto/delete",  
                type:'POST',
                async:true, 
                data: {
                	id_familia : vId
                },
                beforeSend:function(){
                    $(".modal-body p").fadeOut(150,function(){

                    	$(".loading").fadeIn(100);

                    });
                },
                success:function(respuesta){

                    $("#mDelete").modal('hide');

                    $(".loading").fadeOut(150,function(){

                    	$(".modal-body p").fadeIn(100);

                    });

                    if (respuesta !="error") {

                        swal({   title: "Proceso realizado",   text: "Se elimino el registro correctamente",   timer: 3000 ,type: "success"});

                    	location.reload();

                    }else{

                        swal({   title: "Proceso no realizado",   text: "No se pudo eliminar el registro",   timer: 3000 ,type: "info"});

                    }

                    
           
                }
            });

		}
   
	};

	vFamilia.init();
});