$(function() {
	var vProducto = {
		init : function(){
			
          	configTable();

        	vProducto.activeId();

			$("#btn-si").click(function(){

				vProducto.deleteProd();

			});
          
		},
		activeId : function(){

			$(".delete-prod").click(function(){

				var vId = $(this).attr("data");
				$("#id_prod").get(0).value = vId;

			});

		},
		deleteProd : function(){

			var vId = $("#id_prod").get(0).value;

			$.ajax({
                url:"/almacen/producto/delete",  
                type:'POST',
                async:true, 
                data: {
                	id_prod : vId
                },
                beforeSend:function(){
                    $(".modal-body p").fadeOut(150,function(){

                    	$(".loading").fadeIn(100);

                    });
                },
                success:function(respuesta){

                    $("#mDelete").modal('hide');

                    $(".loading").fadeOut(150,function(){

                    	$(".modal-body p").fadeIn(100);

                    });

                    if (respuesta !="error") {

                        swal({   title: "Proceso realizado",   text: "Se elimino el registro correctamente",   timer: 3000 ,type: "success"});

                    	location.reload();

                    }else{
                        
                        swal({   title: "Proceso no  realizado",   text: "No se pudo eliminar el registro",   timer: 3000 ,type: "success"});

                    }

                    
           
                }
            });

		}
   
		
	};

	vProducto.init();
});