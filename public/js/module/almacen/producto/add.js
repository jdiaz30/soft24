$(function() {

    var vForm = null;
    var vProveedor = [];
    var vIgv = "no";
    var vCaja = "";
    var vUnid = "u";
	var vProducto = {
		init : function(){

            var vProv = $("#prov_prod").get(0).value;

            if (vProv !=="") {
                vProveedor = vProv.split(",");
            }

            vForm = init_validate_form("#frmProducto");

            $("#frmProducto").submit(function( event ) {

                event.preventDefault();

                 if ($(".validation-error-label.validation-valid-label").length >= 2) {

                    if($(".btn-registrar").length){

                        var vValida = vProducto.validaCodigoBarras();

                        if (vValida == "0") {

                            $("#cod_barras").parents('.form-group').removeClass("has-error");
                            vProducto.registrar("add");

                        }else{

                            $("#cod_barras").parents('.form-group').addClass("has-error");

                            swal({   title: "Alerta",   text: "El código de barras ya sido registrado con otro producto",   timer: 3000 ,type: "error"});

                        }

                    }else{

                        vProducto.registrar("edit");

                    }

                }

            });

            $(".aProveedor").click(function(e){

                vProducto.addItemTable(this);

            });

            configTable();

            vProducto.deleteItemTable();

            vProducto.fotoInit();
            vProducto.initControls();

            vProducto.checkIgv();

            $("#igv").click(function(){

                vProducto.checkIgv();

            });

            $("#caja").click(function(){

                vProducto.checkCaja();

            });

            $("#unidad").click(function(){

                vProducto.checkUnid();

            });
		},
        registrar : function(proceso){

            var vTipoVenta = vUnid+""+vCaja;

            var vData = $("#frmProducto").serialize();
            vData += "&afecto_igv="+vIgv+"&tipo_venta="+vTipoVenta;

            $.ajax({
                url:"/almacen/producto/"+proceso,  
                type:'POST',
                async:true, 
                data: vData,
                beforeSend:function(){
                    $("#process").modal('show');
                },
                success:function(respuesta){

                    $("#process").modal('hide');

                    swal({   title: "Proceso realizado",   text: "Se guardo correctamente el registro",   timer: 3000 ,type: "success"});

                    if (proceso == "add") {
                        $("#frmProducto").trigger('reset');
                        vProveedor.length = 0;

                        var table = $('#tb_prov').DataTable();
                        table.clear().draw();
                    }

                    vForm.resetForm();

                    setTimeout(function(){

                        location.reload();

                    },2000);
           
                }
            });
        },
        fotoInit : function(){
            var button = $(".btn-foto");
   
            new AjaxUpload(button, {
                action: "/almacen/producto/image-upload",
                onSubmit : function(file , ext){
                if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                    // extensiones permitidas
                    alert('Error: Solo se permiten imagenes');
                    // cancela upload
                    return false;
                } else {
                      
                    this.disable();

                }
                }, 
                onComplete: function(file, response){
                    
                    // habilito upload button                       
                    this.enable();          
                    // Agrega archivo a la lista

                    $("#foto").get(0).value = response;

                    $("#img-prod").attr("src","http://"+document.location.hostname+"/img/productos/"+response);
                   
           
                }   
            });
        },
        addItemTable : function(vThis){

            if (vProveedor.indexOf($(vThis).attr("data-id")) == -1) {

                vProveedor.push($(vThis).attr("data-id"));

                $("#prov_prod").get(0).value = vProveedor;

                $("#tb_prov").DataTable().row.add( [ $(vThis).attr("data-id"), $(vThis).attr("data-logo") , $(vThis).attr("data-razon") ,$(vThis).attr("data-estado"),$(vThis).attr("data-action")]).draw();


            }
        },
        deleteItemTable : function(){

            var table = $('#tb_prov').DataTable();

            $('#tb_prov tbody').on( 'click', '.dropdown-menu.dropdown-menu-right li a', function () {

                var vIdProv = $(this).parents('tr').find("td:first").html();

                var vIndex = vProveedor.indexOf(vIdProv);

                vProveedor.splice(vIndex,1);

                $("#prov_prod").get(0).value = vProveedor;
     
                table
                .row( $(this).parents('tr') )
                .remove()
                .draw();

            } );
        },
        validaCodigoBarras : function(){
            var vRes = "";

            var vCodBarras = $("#cod_barras").get(0).value;

            $.ajax({
                url:"/almacen/producto/valida-codigo-barras",  
                type:'POST',
                async:false, 
                data: {
                    cod_barras : vCodBarras,
                },
                beforeSend:function(){

                },
                success:function(respuesta){

                 vRes = respuesta;
                }
            });

            return vRes;
        },
        initControls : function(){

            $('.money').maskMoney();
        },
        checkIgv : function(){

            $('#igv').change(function() {

                if ($(this).prop('checked')) {

                    vIgv = "si";

                }else{

                    vIgv = "no";

                }

            });
        },
        checkCaja : function(){

            if( $('#caja').is(':checked') ) {
                vCaja = "c";

                $(".caja").css("display","block");

            }else{
                vCaja = "";

                $(".caja").css("display","none");

            }

            $("#cod_barras_c").val("");
            $("#prec_caja").val("");
            $("#und_x_caja").val("");
        },
        checkUnid : function(){

            if( $('#unidad').is(':checked') ) {
                vUnid = "u";

            }else{
                vUnid = "";

            }
        }	
	};

	vProducto.init();
});