<?php

 
class PDF extends \FPDF
{

    function cabeceraReporte($titulo){

        $fecha = date("Y/m/d");

        $this->SetXY(160, 3);
        $this->SetFont('Arial','B',10);

        $this->Cell(0,10,"Fecha : ".$fecha,0,0,'LR');

        $this->SetXY(10, 12);
        $this->SetFont('Arial','B',15);

        $this->Cell(0,10,$titulo,0,0,'C');

        //Salto de línea
        $this->Ln();

    }
   
    function cabeceraTabla($cabecera)
    {

        $this->SetXY(10, 30);
        $this->SetFont('Arial','B',10);
        $this->SetFillColor(2,157,116);//Fondo verde de celda
        $this->SetTextColor(240, 255, 240); //Letra color blanco

        foreach($cabecera as $fila)
        {
            //Atención!! el parámetro true rellena la celda con el color elegido
            $this->CellFitSpace(30,7, utf8_decode($fila),1, 0 , 'L', true);
        }
    }
 
    function datosHorizontal($datos,$apertura)
    {
        $this->SetXY(10,37);
        $this->SetFont('Arial','',10);
        $this->SetFillColor(229, 229, 229); //Gris tenue de cada fila
        $this->SetTextColor(3, 3, 3); //Color del texto: Negro
        $bandera = false; //Para alternar el relleno

        $total = 0;
        foreach($datos as $fila)
        {
            //El parámetro badera dentro de Cell: true o false
            //true: Llena  la celda con el fondo elegido
            //false: No rellena la celda
            $this->CellFitSpace(30,7, utf8_decode($fila['ticket']),1, 0 , 'L', $bandera );
            $this->CellFitSpace(30,7, utf8_decode($fila['f_pago']),1, 0 , 'L', $bandera );
            $this->CellFitSpace(30,7, utf8_decode($fila['tipo_doc']),1, 0 , 'L', $bandera );
            $this->CellFitSpace(30,7, utf8_decode($fila['total']),1, 0 , 'L', $bandera );
            $this->Ln();//Salto de línea para generar otra fila
            $bandera = !$bandera;//Alterna el valor de la bandera

            $total+= $fila['total'];
        }

        $this->Ln();//Salto de línea para generar otra fila

        $this->SetFont('Arial','',12);

        $this->Cell(0,7,"Monto apertura : " . "S/.".$apertura ,0,0,'C');

        $this->Ln();//Salto de línea para generar otra fila

        $this->Cell(0,7,"Total Ventas : " . "S/.".$total ,0,0,'C');
    }
 
    function tablaHorizontal($cabeceraHorizontal, $datosHorizontal,$titulo,$apertura)
    {
        $this->cabeceraReporte($titulo);
        $this->cabeceraTabla($cabeceraHorizontal);
        $this->datosHorizontal($datosHorizontal,$apertura);
    }

    function CellFit($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='', $scale=false, $force=true)
    {
        //Get string width
        $str_width=$this->GetStringWidth($txt);
 
        //Calculate ratio to fit cell
        if($w==0)
            $w = $this->w-$this->rMargin-$this->x;
        $ratio = ($w-$this->cMargin*2)/$str_width;
 
        $fit = ($ratio < 1 || ($ratio > 1 && $force));
        if ($fit)
        {
            if ($scale)
            {
                //Calculate horizontal scaling
                $horiz_scale=$ratio*100.0;
                //Set horizontal scaling
                $this->_out(sprintf('BT %.2F Tz ET',$horiz_scale));
            }
            else
            {
                //Calculate character spacing in points
                $char_space=($w-$this->cMargin*2-$str_width)/max($this->MBGetStringLength($txt)-1,1)*$this->k;
                //Set character spacing
                $this->_out(sprintf('BT %.2F Tc ET',$char_space));
            }
            //Override user alignment (since text will fill up cell)
            $align='';
        }
 
        //Pass on to Cell method
        $this->Cell($w,$h,$txt,$border,$ln,$align,$fill,$link);
 
        //Reset character spacing/horizontal scaling
        if ($fit)
            $this->_out('BT '.($scale ? '100 Tz' : '0 Tc').' ET');
    }
 
    function CellFitSpace($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=false, $link='')
    {
        $this->CellFit($w,$h,$txt,$border,$ln,$align,$fill,$link,false,false);
    }
 
    //Patch to also work with CJK double-byte text
    function MBGetStringLength($s)
    {
        if($this->CurrentFont['type']=='Type0')
        {
            $len = 0;
            $nbbytes = strlen($s);
            for ($i = 0; $i < $nbbytes; $i++)
            {
                if (ord($s[$i])<128)
                    $len++;
                else
                {
                    $len++;
                    $i++;
                }
            }
            return $len;
        }
        else
            return strlen($s);
    }
 
 
} 
?>